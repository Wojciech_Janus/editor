﻿using System;
using System.Collections;

/// <summary>
/// Contains inforamtions abaout image in aplication: name (in images directory) of the texture and date of last modification.
/// </summary>
[Serializable]
public class Image {

	private string textureName;
	private string modDate;

	public void Set(string texturePath, string modDate)
	{
		this.textureName = texturePath;
		this.modDate = modDate;
	}

	public string TextureName
	{
		get { return textureName; }
	}

	public string ModDate
	{
		get { return modDate; }
	}

	public bool IsModDataObsolete(string newDate)
	{

		try
		{
			string[] modDateParts = modDate.Split(new char[]{'-'});
			int modDateYear = int.Parse(modDateParts[0]);
			int modDateMonth = int.Parse(modDateParts[1]);
			int modDateDay = int.Parse(modDateParts[2]);

			DateTime modDateTime = new DateTime(modDateYear, modDateMonth, modDateDay);

			string[] newDateParts = newDate.Split(new char[]{'-'});
			int newDateYear = int.Parse (newDateParts[0]);
			int newDateMonth = int.Parse (newDateParts[1]);
			int newDateDay = int.Parse(newDateParts[2]);

			DateTime newDateTime = new DateTime(newDateYear, newDateMonth, newDateDay);

			//bool status = false;

			return DateTime.Compare(newDateTime, modDateTime) > 0;

			//TODO corect date validation
			/*
			if(newDateYear > modDateYear)
				status = true;
			else if(newDateMonth > modDateMonth)
				status = true;
			else if(newDateDay > modDateDay)
				status = true;

			return status;
*/
		}
		catch{/*TODO exception handling*/ return false;}
	}
}
