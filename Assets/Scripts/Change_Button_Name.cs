﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Change_Button_Name : MonoBehaviour {

	public Text buttonText;

	void Start ()
	{Debug.Log ("Change button text name");
		if (EditorManager.Instance.IsActive == true)
		{
			if (UI.Instance.isFloorPlanEdit == true)
			{
				buttonText.text = "Close";
			}
			else
			{
				buttonText.text = "Back To Menu";
			}
		}
		if (EditorManager.Instance.IsActive == false)
		{
			buttonText.text = "Back To Menu";
		}
	}
}
