﻿using UnityEngine;
using System.Collections;

public class HotSpot_Collision : MonoBehaviour {

	private int count;

	void Update()
	{
		if (EditorManager.Instance.isHotSpotSelected == false)
		{
			rigidbody.velocity = Vector3.zero;
		}
		if (EditorManager.Instance.howManyHSinPoint == 1 || EditorManager.Instance.howManyHSinPoint == 2)
		{
			rigidbody.detectCollisions = false;
		}
		else 
		{
			rigidbody.detectCollisions = true;
		}
	}
	/*
	void OnCollisionStay (Collision collision)
	{
		if (EditorManager.Instance.isHotSpotSelected == false)
		{
			Debug.Log ("collision detected");
			Debug.Log ("collision name " + collision.gameObject.name);
			if (collision.gameObject.name == "HotSpot(Clone)")
			{
				rigidbody.velocity = Vector3.zero;
			}
		}
	}
	*/
}
