﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeleteButton : Interactive
{
	public Text text;

	public override void OnChoose()
	{
		Debug.Log ("delete button hit");
		text.text = "Deleting...";
	}
	
	public override void OnHold (){}
	public override void OnNotHold (){}
	
	public override void OnHover ()
	{
		text.color = Color.blue;
	}
	
	public override void OnStopHover ()
	{
		text.color = Color.black;
	}
}
