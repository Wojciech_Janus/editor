﻿using UnityEngine;
using System.Collections;

public class ActivateButton : MonoBehaviour {

	public GameObject button1;
	public GameObject button2;

	void Start ()
	{
		if (EditorManager.Instance.IsActive == true)
		{
			if (UI.Instance.isFloorPlanEdit == true)
			{
				//saveButton.SetActive (true);
				button1.SetActive (false);
				button2.SetActive (false);
			}
			else if (UI.Instance.isFloorPlanEdit == false)
			{
				//saveButton.SetActive (false);
				button1.SetActive (true);
				button2.SetActive (true);
			}
		}
		if (EditorManager.Instance.IsActive == false && UI.Instance.isFloorPlanEdit == false)
		{
			//saveButton.SetActive (false);
			button1.SetActive (true);
			button2.SetActive (true);
		}
	}
}
