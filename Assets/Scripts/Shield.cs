﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

	private Vector3 shieldPosition;

	void Start () 
	{
		shieldPosition=transform.position;
		Color colorSprite=Color.gray;
		colorSprite.a=0.9f;
		GetComponent<SpriteRenderer>().material.color=colorSprite;
	}

	void Update () 
	{
		transform.position=shieldPosition;
	}
}
