﻿using UnityEngine;
using System.Collections;
using System;

public class VRButton : Interactive {

	private const float HOVER_SIZE = 1.1f;

	private GameObject window;
	private Action chooseAction;

	public void Set(GameObject window, Action chooseAction)
	{
		this.window = window;
		this.chooseAction = chooseAction;
	}

	public override void OnChoose ()
	{
		chooseAction();
		Close ();
	}

	public override void OnHold (){}
	public override void OnNotHold (){}

	public override void OnHover ()
	{
		transform.localScale = Vector3.one * HOVER_SIZE;
	}

	public override void OnStopHover ()
	{
		transform.localScale = Vector3.one;
	}

	private void Close()
	{
		UI.Instance.isSomeWindowOpen = false;

		window.SetActive(false);
		DestroyImmediate(window);
	}
}
