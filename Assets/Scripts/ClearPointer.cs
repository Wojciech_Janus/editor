﻿using UnityEngine;
using System.Collections;

public class ClearPointer : MonoBehaviour {
	
	private Transform[] hold;

	void Start () 
	{
		StartCoroutine (SetFree ());
	}

	private IEnumerator SetFree()
	{
		while (true)
		{
			if (UI.Instance.clearPoint == true)
			{
				foreach (Transform gm in transform)
				{
					if (gm.name == "HotSpot(Clone)")
					{
						gm.gameObject.transform.SetParent (null);
					}
				}
				UI.Instance.clearPoint = false;
			}

			int i = gameObject.GetComponentsInChildren<HotSpot> ().Length;
			EditorManager.Instance.howManyHSinPoint = i;
			if (i > 1)
			{
				Debug.Log ("get components in child HotSpot Length is (>1)" + i);
				foreach (Transform gm in transform)
				{
					if (gm.name == "HotSpot(Clone)")
					{
						gm.gameObject.transform.SetParent (null);
					}
				}
			}
			yield return null;
		}
		yield return null;
	}
}
