using UnityEngine;
using System.Collections;

public enum TouchAction
{
	SingleTap, DoubleTap, Left, Right, Up, Down, Hold, None
};

/// <summary>
/// Touchpad manager. Set flags abaout current tocuh status or gestures.
/// </summary>
public class TouchInput : MonoBehaviour
{
	private static TouchAction touchAction = TouchAction.None;

	#region PROPERTIES
	public static Vector3 MoveAmount
	{
		get { return OVRTouchpad.NormalizedMoveAmountMouse; }
	}

	public static Vector3 DeltaPosition
	{
		get { return OVRTouchpad.NormalizedMouseDeltaPosition; }
	}

	public static bool SingleTap
	{
		get { return touchAction == TouchAction.SingleTap; }
	}

	public static bool DoubleTap
	{
		get { return touchAction == TouchAction.DoubleTap; }
	}

	public static bool Left
	{
		get { return touchAction == TouchAction.Left; }
	}

	public static bool Right
	{
		get { return touchAction == TouchAction.Right; }
	}

	public static bool Up
	{
		get { return touchAction == TouchAction.Up; }
	}

	public static bool Down
	{
		get { return touchAction == TouchAction.Down; }
	}

	public static bool Hold
	{
		get { return touchAction == TouchAction.Hold; }
	}
	#endregion

	void Awake()
	{
		OVRTouchpad.TouchHandler += TouchEventCallback;
	}

	public void TouchEventCallback(object sender, System.EventArgs args)
	{
		var touchArgs = (OVRTouchpad.TouchArgs)args;
		OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;

		touchAction = (TouchAction)((int)touchEvent);

<<<<<<< HEAD
		//if(NetworkManager.Instance.IsAnyClientConnect)
=======
		//if(NetworkManager.Instance.IsAnyClientConnect)
>>>>>>> master2
        //	networkView.RPC("SetFlagsRemotely", RPCMode.All, (int)touchAction);
		//else
			SetFlagsRemotely((int)touchAction);
	}

    [RPC]
<<<<<<< HEAD
    private void SetFlagsRemotely(int n)
    {
        touchAction = (TouchAction)n;
        StartCoroutine(ReturnToNoneAfterThisFrame());
=======
    private void SetFlagsRemotely(int n)
    {
        touchAction = (TouchAction)n;
        StartCoroutine(ReturnToNoneAfterThisFrame());
>>>>>>> master2
    }

	private IEnumerator ReturnToNoneAfterThisFrame()
	{
		yield return new WaitForEndOfFrame();
		touchAction = TouchAction.None;
	}
}

