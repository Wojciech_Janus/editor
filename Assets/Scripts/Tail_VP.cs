using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tail_VP : Interactive {

	//public static bool hoverScaleSet = false;

	public int currentTail;

	private const float NORMAL_SCALE_SCALAR = 1.0f;
	private const float HOVER_SCALE_SCALAR = 1.1f;
	public bool isOpened = false;

	private System.Action<int> OnChooseAction;

	[SerializeField]
	private Text nameText;
	[SerializeField]
	private Text priceText;
	[SerializeField]
	private GameObject myTailsMenu;
	[SerializeField]
	private Renderer imageRenderer;
	private Material material;
	public int index;
	private int tailHover;		
	private int lastIndex;
	private ScrollingMenu scrollingMenu;
	public Animator anim;

	void Start()
	{
		anim = GetComponent<Animator> ();
		scrollingMenu = ScrollingMenu.GetMenu ();
	}

	public void Init(int index, Transform myTailsMenu, System.Action<int> OnChooseAction, string name, Texture texture)
	{
		Debug.Log ("Created Tail with index: " + index + " and name: " + name);//menu tail in room
		this.index = index;
		this.myTailsMenu = myTailsMenu.gameObject;
		this.OnChooseAction += OnChooseAction;
		this.nameText.text = name;
		this.CloneMaterialAndSetTexture(texture);
		lastIndex=index;
	}

	public override void OnChoose ()
	{

			//Debug.Log("EDIT MODE IS: "+EditorManager.Instance.isEdit);
			//EditorManager.Instance.isEditButtonDelete = true;
			OnChooseAction(index);
			currentTail = index;
			Close();
	}

	public override void OnHold (){}

	public override void OnHover ()
	{
		transform.localScale = Vector3.one * HOVER_SCALE_SCALAR;
		//hoverScaleSet = true;
	}
	
	public override void OnNotHold (){}

	public override void OnStopHover ()
	{
		transform.localScale = Vector3.one * NORMAL_SCALE_SCALAR;
		//hoverScaleSet = false;
	}

	private void CloneMaterialAndSetTexture(Texture texture)
	{
		material = new Material(imageRenderer.material);
		material.mainTexture = texture;
		imageRenderer.material = material;
	}

	public void Close()//private
	{
		Debug.Log ("Now i will close the VP rendering list");
		UI.Instance.isSomeWindowOpen = false;
		if (myTailsMenu.name.Contains ("ScrollingMenu")) 
		{
			Debug.Log (">>>Close ScrollingMenu<<<");
			UI.Instance.isListingMenuOpen = false;
		}
		myTailsMenu.SetActive(false);
		Destroy(myTailsMenu);
	}
}
