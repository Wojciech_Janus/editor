﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class QuestionBoxView : MonoBehaviour {

	//public VRButton closeAndSave;
	public VRButton yes;
	public VRButton no;
	public Text question;
		
	public static GameObject Inst(Action OnYesV, Action OnCancel, string question)
	{
		GameObject questionBoxGO = Instantiate(UI.Instance.questionBoxPrefabView) as GameObject;
		QuestionBoxView questionBox = questionBoxGO.GetComponent<QuestionBoxView>();
			
		questionBox.yes.Set(questionBoxGO, OnYesV);
		questionBox.no.Set(questionBoxGO, OnCancel);
		questionBox.question.text = question;
			
		return questionBoxGO;
	}
		
	void Update()
	{
		/*if(Input.GetKeyDown(KeyCode.Escape))
		{
			gameObject.SetActive(false);
			DestroyImmediate(gameObject);
		}*/
	}
}
