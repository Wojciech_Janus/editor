﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TakeTourButton : Interactive
{
	public GameObject textToDeactivate;
	private Button button;
	[SerializeField]
	private Color colorButton;
	[SerializeField]
	private Text textOnHover;
	
	void OnEnable()
	{
		button = GetComponent<Button> ();
	}

	public override void OnChoose()
	{
		UI.Instance.buttonEditMode.SetActive (false);
<<<<<<< HEAD
		EditorManager.Instance.nowIndexOfFloorPlan = 1;
=======
>>>>>>> master2
		transform.parent.parent.GetComponent<Tail> ().TakeTour ();
	}

	public override void OnHold (){}
	public override void OnNotHold (){}

	public override void OnHover ()
	{
		ColorBlock colorBlock = button.colors;
		colorBlock.normalColor = colorButton;
		button.colors = colorBlock;
		textToDeactivate.SetActive(false);
		textOnHover.gameObject.SetActive(true);
		textOnHover.color = colorButton;
	}

	public override void OnStopHover ()
	{
		ColorBlock colorBlock = button.colors;
		colorBlock.normalColor = Color.white;
		button.colors = colorBlock;
		textToDeactivate.SetActive(true);
		textOnHover.gameObject.SetActive(false);
		textOnHover.color = Color.white;
	}
}
