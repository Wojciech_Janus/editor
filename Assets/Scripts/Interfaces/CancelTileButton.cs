﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CancelTileButton : Interactive
{
	private Button button;
	
	void OnEnable()
	{
		button = GetComponent<Button> ();
	}

	public override void OnChoose()
	{
		if(transform.parent.parent.GetComponent<Tail> ().isOpened)
		{
			transform.parent.parent.GetComponent<Tail> ().anim.SetBool ("animation", false);
			ScrollingMenu.GetMenu ().MoveAside (false);
		}
	}
	
	public override void OnHold (){}
	public override void OnNotHold (){}
	
	public override void OnHover ()
	{
		transform.localScale = new Vector3 (0.011f, 0.011f, 1);		
	}
	
	public override void OnStopHover ()
	{
		transform.localScale = new Vector3 (0.01f, 0.01f, 1);
	}
}