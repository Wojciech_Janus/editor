﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class which is using to take events from Pointer.
/// </summary>
[System.Serializable]
public abstract class Interactive : MonoBehaviour{

	public abstract void OnHover();
	public abstract void OnStopHover();
	public abstract void OnChoose();
	public abstract void OnHold();
	public abstract void OnNotHold();
}
