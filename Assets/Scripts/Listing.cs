﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;//to add MonoBehaviour
/// <summary>
/// Listing representation. Full description data and access to helded Vantage Points.
/// </summary>
[Serializable]
public class Listing {
	
	private Dictionary<int, VantagePoint> vantagePoints;
	#region informations
	public Agent agent;
	public int Id;	
	public int startingVantagePointId;
	public string name;
	public string description;
	public string price;
	public int area;
	public string zipCode;
	public string street;
	public string country;
	public string staus;
	#endregion informations
	
	public void Set(int id, Dictionary<int, VantagePoint> vantagePoints)
	{
		this.Id = id;
		this.vantagePoints = vantagePoints;
	}
	
	public Dictionary<int, VantagePoint> VantagePoints
	{
		get { return vantagePoints; }
	}
	
	public VantagePoint VantagePointAt(int i)
	{
		int n = 0;
		
		foreach(VantagePoint vp in vantagePoints.Values)
		{
			if(n == i)
			{
				return vp;
			}
			else
			{
				++n;
			}
		}
		
		return null;
	}
	
	public VantagePoint StartingVantagePoint
	{
		get { return vantagePoints[startingVantagePointId]; }
	}
	
	public int VantagePointsNumber
	{
		get { return vantagePoints.Count; }
	}
	
	public bool ContainsVantagePoint(int id)
	{
		return vantagePoints.ContainsKey(id);
	}
	
	public override string ToString ()
	{
		return Id + 
			"\n" + name +
				"\n" + description +
				"\n" + price +
				"\n" + area +
				"\n" + zipCode +
				"\n" + street +
				"\n" + country;
	}
}
