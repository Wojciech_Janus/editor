﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloorPlan : Interactive {

	[SerializeField]
	private Text floorPlanPointName;
	private FloorPlanPoint data;
	private bool oneShoot = true;
	private SpriteRenderer spriteRenderer;
	private Sprite notSelected;

	void OnEnable()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public FloorPlanPoint Data
	{
		get { return data; }
	}

	public void SetData(FloorPlanPoint data)
	{
		this.data = data;
	}

	public override void OnChoose ()
	{
		if (data.linkedVP == null)
		{
			Debug.Log("FloorPlan data.linkedVP = null");
		}
		else
		{
			//FloorPlan Edit
			if (EditorManager.Instance.IsActive == true && UI.Instance.isFloorPlanEdit == true)
			{
				Debug.Log ("OnPointerRaycastHit - floorplan edit mode is on, create hotspot");
				Debug.Log ("HotSpot linkedVP Id = "+data.linkedVP.Id);
				EditorManager.Instance.CreateHotSpot(EditorManager.Instance.hitPosition, data.linkedVP);
				EditorManager.Instance.isFloorPlanOnScene = false;
				Destroy(EditorManager.Instance.lastFloorPlan);
			}
			//FloorPlan View
			else if ((EditorManager.Instance.IsActive == true && UI.Instance.isFloorPlanEdit == false) || (EditorManager.Instance.IsActive == false))
			{
				EditorManager.Instance.RayCastOn = true;
				EditorManager.Instance.windowForEdit = true;
				EditorManager.Instance.isFloorPlanOnScene = false;
				Debug.Log("You choose a VP "+data.linkedVP);
				data.linkedVP.GoTo();
				EditorManager.Instance.windowForView = true;
				Destroy(EditorManager.Instance.lastFloorPlan);
			}
		}
	}
	
	public override void OnHover ()
	{
		spriteRenderer.sprite = UI.Instance.select;
	}
	
	public override void OnStopHover ()
	{
		spriteRenderer.sprite = notSelected;
	}
	
	public override void OnHold (){}
	public override void OnNotHold (){}

	public void SetNameText(string name)
	{
		floorPlanPointName.text = name + " " + data.linkedVP.Id;
	}

	public void SetSprite(Sprite spriteToSet)
	{
//		Debug.Log(">>> FloorPlan SetSprite <<< spriteToSet: " + spriteToSet.name);
		spriteRenderer.sprite = spriteToSet;
		notSelected = spriteRenderer.sprite;
	}
}
