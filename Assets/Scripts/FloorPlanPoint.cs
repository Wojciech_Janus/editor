﻿/// <summary>
/// Represent FloorPlan serializable information.
/// </summary>

[System.Serializable]
public class FloorPlanPoint {
	
	public float x;
	public float y;
	public float z;
	public VantagePoint linkedVP;
	
	public FloorPlanPoint (float x, float y ,float z, VantagePoint linkedVP)
	{ 
		this.x = x;
		this.y = y;
		this.z = z;
		this.linkedVP = linkedVP;
	}
}