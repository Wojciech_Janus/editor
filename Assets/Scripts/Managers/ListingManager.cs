﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Http;

public class ListingManager : MonoBehaviour {

	public static List<string> listingsToViewIds = new List<string>();
	//public List<FloorPlanPoint> floorPlanData = new List<FloorPlanPoint>();
<<<<<<< HEAD
	public List<List <List <List <HotSpotData>>>> allHotSpotsList = new List<List <List <List <HotSpotData>>>> ();
=======
>>>>>>> master2
	public List<HotSpotData> oldHotSpotList = new List<HotSpotData>();//hotspots DATA list loaded from database (in VantagePoint.cs)
	public List<HotSpotData> hotSpotsDeletedList = new List<HotSpotData>();
	public List<HotSpot> hotSpotsList = new List<HotSpot>();
	public Dictionary<int, Listing> listingsDictionary = new Dictionary<int, Listing>();
	public Listing currentListing;
	public VantagePoint currentVantagePoint;

	private static ListingManager instance;
	private const string path = DataBaseCommunicator.SERVER_URL + DataBaseCommunicator.DATABASE_APPLICATION_PORT + HOTSPOT_PATH;
	private const string HOTSPOT_PATH = "/vantage_point/hotspot/";

	private int hs_index = 0;
	//ADD: vantage_points[myVantagePoint.Id].vantage_point_hotspots[count+1].vantage_point_id
	//vantage_points[myVantagePoint.Id].vantage_point_hotspots[count+1].id
	//vantage_point_destination_id



	//vantage_point_id = currentVP
	//vantage_point_destination_id
	
	private int LISTING_ID_KEY = 6;

	public static ListingManager Instance
	{
		get { return instance; }
	}

	void Awake()
	{
		if(instance == null)
			instance = this;
	}

	void Update ()
	{
//		Debug.Log (">>>>>>>>>>>>>>"+DataBaseCommunicator.Instance.idHotSpot.Count+"<<<<<<<<<<<<<<<");
	}

	public Listing ListingAt(int i)
	{
		int n = 0;
		foreach(Listing listing in listingsDictionary.Values)//listing = Vantage Point
		{
			if(n == i)
			{
				return listing; 
			}
			else
			{
				++n;
			}
		}
		return null;
	}

	public int ListingIndex(int id)
	{
		Debug.Log(">>> SEARCH LISTING ID <<< " + id);
		int n = 0;

		foreach(Listing listing in listingsDictionary.Values)
		{
			Debug.Log (listing.Id);
		}
		foreach(Listing listing in listingsDictionary.Values)
		{
			Debug.Log ("current index: " + n + "currentID: " + listing.Id);
			if(id == listing.Id)
			{
				Debug.Log ("Found in " + n);
				return n;
			}
			++n;
		}
		return -1;
	}

	public void PopulateListingsDictionary()
	{
		string[] listingsNames = StorageManager.Instance.FilesInDirectory("saves", "bin");
		int numberOfListings = listingsNames.Length;
		for(int i = 0; i < numberOfListings; i++)
		{
			Listing currentListing = StorageManager.Instance.LoadObject<Listing>(listingsNames[i]);
//			Debug.Log("listinsName" + listingsNames[i]);//add tail
			listingsDictionary.Add(currentListing.Id, currentListing);
		}
	}

	public void AddListingsToList(string[] listings)
	{
		foreach(string id in listings)
		{
			Debug.Log ("Add listing: " + id + "to litsitngs list");
			listingsToViewIds.Add(id);
		}
	}

	public void LoadToRAMCurrentListingRenders()
	{
		RenderManager.Instance.ClearRendersList ();
		Debug.Log (">>> LoadToRAMCurrentListingRenders() <<< VP in current listing: " + currentListing.VantagePoints.Values.Count);
<<<<<<< HEAD
		int i = 0;
		foreach(VantagePoint vp in currentListing.VantagePoints.Values)
		{
			i++;
			vp.Sphere.LoadToRAM();//add sphere (mesh)
		}
		Debug.Log("ALL VP = " + i);
=======
		foreach(VantagePoint vp in currentListing.VantagePoints.Values)
		{
			vp.Sphere.LoadToRAM();//add sphere (mesh)
		}
>>>>>>> master2
	}

	public VantagePoint SetCurrentVantagePoint(VantagePoint vp)
	{
		currentVantagePoint = vp;
		return currentVantagePoint;
	}

	public void SetCurrentListing(Listing listing)
	{
		currentListing = listing;
		Debug.Log(listing);
	}

	public VantagePoint SetCurrentVantagePoint(int id)
	{
		currentVantagePoint = currentListing.VantagePoints[id];
		return currentVantagePoint;
	}
	
	public void AddToHotSpotsList(HotSpot hotSpot)
	{
		hotSpotsList.Add(hotSpot);
		Debug.Log("Add hotspot to scene (clone)"+hotSpot);
	}
	
	public void ClearHotSpotsList()
	{
		foreach(HotSpot hotSpot in hotSpotsList)
		{
			Debug.Log ("hotspot id = " + hotSpot.Data.id);
			Destroy(hotSpot.gameObject);
			Debug.Log("deleted");
		}
		hotSpotsList.Clear();
	}
	
	public IEnumerator SaveListings()
	{
<<<<<<< HEAD
		//foreach (HotSpot hotSpot in hotSpotsList)//hotSpotsList = all hotspots in current vantagepoint (old and new)
		//{
//			Debug.Log ("HS ID in hotSpotsList = " + hotSpot.Data.id);
			//yield return StartCoroutine(CheckHotSpotToAdd(hotSpot.Data));//save hotspots
		//}

		foreach (List <List<HotSpotData>> list_test  in allHotSpotsList[DataBaseCommunicator.Instance.currentListingIndex])
		{
			foreach (List <HotSpotData> vp_test in list_test)
			{
				foreach (HotSpotData test in vp_test)
				{
					Debug.Log(">>>HOTSPOTDATA IN LIST<<< " + test.id);
					yield return StartCoroutine(CheckHotSpotToAdd(test));//save hotspots
				}
			}
		}

=======
		foreach (HotSpot hotSpot in hotSpotsList)//hotSpotsList = all hotspots in current vantagepoint (old and new)
		{
			Debug.Log ("HS ID in hotSpotsList = " + hotSpot.Data.id);
			yield return StartCoroutine(CheckHotSpotToAdd(hotSpot.Data));//save hotspots
		}
>>>>>>> master2
		foreach (Listing currentListing in listingsDictionary.Values)
		{
			StorageManager.Instance.SaveObject<Listing>(currentListing, currentListing.Id.ToString());
			yield return null;
		}
<<<<<<< HEAD
		hs_index = 0;
=======
		foreach (HotSpotData data in oldHotSpotList)
		{
			Debug.Log ("HS ID in oldHotSpotList =  " + data.id);
		}
		hs_index = 0;
		/*
		foreach (HotSpot hotspot in oldHotSpotList)
		{
			//check all hotspot in all listing
			Debug.Log ("checking");
			Debug.Log ("hotspot.Data.myVantagePoint.Id = " + hotspot.Data.myVantagePoint.Id);
			Debug.Log ("currentListing.Id = " + currentListing.Id);
			if (hotspot.Data.myVantagePoint.Id == currentListing.Id)
			{
				Debug.Log ("inside");
			}
		}
		*/
>>>>>>> master2
	}

	private IEnumerator CheckHotSpotToAdd(HotSpotData hotspotDataFromList)//save hotspots to database
	{
<<<<<<< HEAD
//		Debug.Log ("HotSpot x = " + hotspotDataFromList.x);
//		Debug.Log ("HotSPot y = " + hotspotDataFromList.y);
//		Debug.Log ("HotSpot distance = " + hotspotDataFromList.distance);
//		Debug.Log ("HotSpot current VP = " + hotspotDataFromList.myVantagePoint.Id);
//		Debug.Log ("HotSpot destinaton VP = " + hotspotDataFromList.linkedVantagePoint.Id);
//		Debug.Log ("HotSpot ID " + hotspotDataFromList.id);
=======
		Debug.Log ("HotSpot x = " + hotspotDataFromList.x);
		Debug.Log ("HotSPot y = " + hotspotDataFromList.y);
		Debug.Log ("HotSpot distance = " + hotspotDataFromList.distance);
		Debug.Log ("HotSpot current VP = " + hotspotDataFromList.myVantagePoint.Id);
		Debug.Log ("HotSpot destinaton VP = " + hotspotDataFromList.linkedVantagePoint.Id);
		Debug.Log ("HotSpot ID " + hotspotDataFromList.id);
>>>>>>> master2
		if (hotSpotsDeletedList.Count != 0)
		{
			foreach (HotSpotData hotSpotData in hotSpotsDeletedList)
			{
				Debug.Log ("HotSpot deleted");
				if (hotSpotData.id == 0)
				{
					Debug.Log ("HotSpot has invalid id (0)");
				}
				else
				{
					yield return StartCoroutine ( DeleteHotSpot (hotSpotData) );
				}
			}
			Debug.Log ("Clear - HotSpots Deleted List");
			hotSpotsDeletedList.Clear ();
		}

		if (oldHotSpotList.Contains (hotspotDataFromList))//hotspot was in database, oldHotSpotList => all hotspots in all listings
		{
			Debug.Log("HotSpot was in DataBase");
			yield return StartCoroutine ( UpdateHotSpot (hotspotDataFromList) );		//update hotspot
		}
		else 													//hotspot is new
		{	
			Debug.Log("HotSpot is New");
			yield return StartCoroutine ( AddHotSpotToDataBase (hotspotDataFromList) );//addhotspot to database
		}
		hs_index++;
		yield return null;
	}

	private IEnumerator DeleteHotSpot(HotSpotData hotspotDataToDelete)
	{
		oldHotSpotList.Remove (hotspotDataToDelete);
		Debug.Log ("Token = " + DataBaseCommunicator.Instance.Token);
		Debug.Log("Now I Delete The HotSpot From DataBase");
		Debug.Log ("VantagePoint ID (current) " + hotspotDataToDelete.myVantagePoint.Id);
		Debug.Log ("VantagePoint destination " + hotspotDataToDelete.linkedVantagePoint.Id);
		Debug.Log ("HotSpot ID " + hotspotDataToDelete.id);
		HttpRequest postRequest = new HttpRequest (HttpMethod.DELETE, path + hotspotDataToDelete.id, DataBaseCommunicator.Instance.Token);//, token);
		yield return null;
		string feedback = postRequest.Send();
		Debug.Log (">>>DELETE - END<<<");
<<<<<<< HEAD
=======
		Debug.Log ("Delete HotSpot is ON");
>>>>>>> master2
		yield return null;
	}

	private IEnumerator UpdateHotSpot(HotSpotData hotspotDataToCheck)
	{
		//update vantage_point/hotspot/id
		//delete vantage_point/hotspot/id
		Debug.Log("Now I Update The HotSpot And Send To The DataBase");
		//Debug.Log ("VantagePoint ID (current) " + hotspotDataToCheck.myVantagePoint.Id);
		//Debug.Log ("VantagePoint destination " + hotspotDataToCheck.linkedVantagePoint.Id);
		Debug.Log ("HotSpot ID " + hotspotDataToCheck.id);
		if (hotspotDataToCheck.id == 0)
		{
			Debug.Log ("hs index = " + hs_index);

			foreach (int id in DataBaseCommunicator.Instance.idHotSpot)
			{
				Debug.Log ("id in list " + id);
			}

			Debug.Log ("HS ID = 0 then HS was new!");
			hotspotDataToCheck.id = DataBaseCommunicator.Instance.idHotSpot[hs_index];
			Debug.Log ("HS ID after change = " + hotspotDataToCheck.id);
			Debug.Log ("Now Change HS ID in list");

			foreach (HotSpotData data in oldHotSpotList)
			{
				if (data.id == 0)
				{
					Debug.Log ("Find in oldHotSpotList HS ID = 0");
					Debug.Log ("Change the HS ID to: " + DataBaseCommunicator.Instance.idHotSpot[hs_index]);
					data.id = DataBaseCommunicator.Instance.idHotSpot[hs_index];
					Debug.Log ("Now I will change variables of this HS");
					data.x = hotspotDataToCheck.x;
					data.y = hotspotDataToCheck.y;
					data.z = hotspotDataToCheck.z;
					data.distance = hotspotDataToCheck.distance;
				}
			}
		}
		//get id from database
		//HttpRequest postRequest = new HttpRequest(HttpMethod.POST, path + hotspotDataToCheck.myVantagePoint.Id, DataBaseCommunicator.Instance.Token);//, token);
		//yield return null;


		HttpRequest postRequest = new HttpRequest (HttpMethod.PATCH, path + hotspotDataToCheck.id, DataBaseCommunicator.Instance.Token);//, token);
		yield return null;
		postRequest.AddField ( "angle_x" ,hotspotDataToCheck.x );
		postRequest.AddField ( "angle_y" ,hotspotDataToCheck.y );
		postRequest.AddField ( "distance" ,hotspotDataToCheck.distance );
		postRequest.AddField ( "vantage_point_id" ,hotspotDataToCheck.myVantagePoint.Id );
		postRequest.AddField ( "vantage_point_destination_id" ,hotspotDataToCheck.linkedVantagePoint.Id );
		string feedback = postRequest.Send();
		Debug.Log ("Update HotSpot feedback: " + feedback);
		Debug.Log (">>>UPDATE - END<<<");
<<<<<<< HEAD
=======
		Debug.Log ("Update HotSpot is ON");
>>>>>>> master2
		yield return null;
	}

	private IEnumerator AddHotSpotToDataBase(HotSpotData hotspotDataToAdd)
	{
		oldHotSpotList.Add (hotspotDataToAdd);
		Debug.Log ("Now I Add a New HotSpot To DataBase and oldHotSpotList");
		Debug.Log ("VantagePoint ID (current) " + hotspotDataToAdd.myVantagePoint.Id);
		Debug.Log ("VantagePoint destination " + hotspotDataToAdd.linkedVantagePoint.Id);
		Debug.Log("x = "+hotspotDataToAdd.x);
		Debug.Log("y = "+hotspotDataToAdd.y);
		Debug.Log("distance = "+hotspotDataToAdd.distance);
		HttpRequest postRequest = new HttpRequest(HttpMethod.POST, path + hotspotDataToAdd.myVantagePoint.Id, DataBaseCommunicator.Instance.Token);//, token);
		yield return null;
		postRequest.AddField("angle_x" ,hotspotDataToAdd.x);
		postRequest.AddField("angle_y" ,hotspotDataToAdd.y);
		postRequest.AddField("distance" ,hotspotDataToAdd.distance);
		postRequest.AddField ( "vantage_point_id" ,hotspotDataToAdd.myVantagePoint.Id );
		postRequest.AddField ( "vantage_point_destination_id" ,hotspotDataToAdd.linkedVantagePoint.Id );
		string feedback = postRequest.Send();
		Debug.Log ("Add HotSpot feedback: " + feedback);
		Debug.Log (">>>ADD - END<<<");
<<<<<<< HEAD
=======
		Debug.Log ("Add HotSpot is ON");
>>>>>>> master2
		yield return null;
	}
}


