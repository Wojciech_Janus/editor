﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Manage HotSpots editing using OnRaycastHit event from Pointer class.
/// </summary>
public class EditorManager : MonoBehaviour {
	
	//public List<FloorPlanPoint> floorPlanData = new List<FloorPlanPoint>();
	public List< GameObject> list_points = new List<GameObject> ();
	public List< List <FloorPlanPoint> > floorPlanData = new List< List <FloorPlanPoint> > ();
<<<<<<< HEAD
	public bool isHotSpotSelected = false;
=======
>>>>>>> master2
	public GameObject hotSpotPrefab;		
	public GameObject VantagePointOnFP;			//point on FloorPlan
	public GameObject ui;						//to set all in one UI
	public GameObject lastFloorPlan;			//to open current floorplan
	public GameObject deleteZone;				//zone to kill hotspot
	public bool windowForEdit = false;			//to choose windows QuestionBoxEdit
	public bool windowForView = false;			//to choose windows QuestionBoxView
	public bool ModeEditON = true;				//MODE: true = EDIT MODE false = VIEW MODE
	public bool isFloorPlanOnScene = false; 	//for only one floorplan at the same time
	public bool RayCastOn = true;
	public bool startFloorPlan = false;			//to set color when create floorplan
	public Vector3 hitPosition;
<<<<<<< HEAD
	public int howManyHSinPoint = 0;
	public int nowIndexOfFloorPlan = 1;
	
	private static EditorManager instance;
	private bool isActive = false;
=======

	public int nowIndexOfFloorPlan = 0;

	private static EditorManager instance;
	private bool isActive;
>>>>>>> master2
	private Pointer pointer;

	[SerializeField]
	private HotSpot tempHotSpot;
	
	public static EditorManager Instance
	{
		get { return instance; }
	}
	
	public bool IsActive
	{
		get { return isActive; }
	}
	
	void Awake()
	{
		if(instance == null)
			instance = this;
		pointer = Pointer.Instance;
		pointer.OnRaycastHit += OnPointerRaycastHit;
<<<<<<< HEAD
		deleteZone.SetActive (false);
=======
		Deactivate();
>>>>>>> master2
	}

	void Update()																//EDIT MODE OR VIEW MODE
	{
		if (isActive == true)													//EDIT MODE
		{
			if (Input.GetKeyDown(KeyCode.Escape) && windowForEdit == true && isFloorPlanOnScene == false)
			{Debug.Log("create floorplan (vlac)");
				RayCastOn = false;
				windowForEdit = false;
				UI.Instance.isFloorPlanEdit = false;							
				UI.Instance.ShowFloorPlan();									//show FloorPlan in edit mode
				startFloorPlan = true;
				isFloorPlanOnScene = true;
			}
			else if (Input.GetKeyDown(KeyCode.Escape) && windowForEdit == false)
			{Debug.Log("destroy floorplan in edit mode");
				windowForEdit = true;
				RayCastOn = true;
				Destroy(EditorManager.Instance.lastFloorPlan);					//destroy FloorPlan in edit mode
				isFloorPlanOnScene = false;
			}
			else if (Input.GetKeyDown(KeyCode.Escape) && isFloorPlanOnScene == true)//close red floorplan
			{
				windowForEdit = true;
				RayCastOn = true;
				Destroy(EditorManager.Instance.lastFloorPlan);					//destroy FloorPlan in edit mode
				isFloorPlanOnScene = false;
			}
		}

		if (isActive == false)													//VIEW MODE
		{
			if (Input.GetKeyDown(KeyCode.Escape) && windowForView == true)
			{Debug.Log("create floorplan in view mode");
				RayCastOn = false;
				windowForView = false;
				UI.Instance.ShowFloorPlan();									//show FloorPlan in view mode
				startFloorPlan = true;
			}
			else if (Input.GetKeyDown(KeyCode.Escape) && windowForView == false)
			{Debug.Log("destroy floorplan in view mode");
				RayCastOn = true;
				windowForView = true;
				Destroy(EditorManager.Instance.lastFloorPlan);					//destroy FloorPlan in view mode
			}
		}
	}
	
	public void OnPointerRaycastHit(Vector3 hitPosotion)
	{
		if (isActive == true && isFloorPlanOnScene == false && RayCastOn == true)// && UI.Instance.isFloorPlanEdit == false)
		{
			Debug.Log ("Click in EditMode (raycasthit)");
			//windowForView = false;
			isFloorPlanOnScene = true;
			UI.Instance.isFloorPlanEdit = true;				//to change floorplan for create hotspot on click
			hitPosition = hitPosotion;
			UI.Instance.ShowFloorPlan();					//for edit,same
			startFloorPlan = true;
		}
		/*
		Debug.Log ("OnPointerRaycastHit");
		if(isActive)
		{
			if(!HotSpot.isOpenChooseMenu)
				CreateHotSpot(hitPosotion);
		}
		*/
	}
	
	public void CreateHotSpot(Vector3 hotSpotPosition, VantagePoint linkedID)//private
	{
		float distance = 9.0f;
		GameObject hotSpotGO = Instantiate(hotSpotPrefab) as GameObject;
		tempHotSpot = hotSpotGO.GetComponent<HotSpot>();
		tempHotSpot.Init(hotSpotPosition, distance, ListingManager.Instance.currentVantagePoint);
		tempHotSpot.Data.linkedVantagePoint = linkedID;
		ListingManager.Instance.currentVantagePoint.AddHotSpotData(tempHotSpot.Data);
		tempHotSpot.GoToDestination();//create Tail_VP, add hotspot to list
	}
	
<<<<<<< HEAD
=======
	public void Deactivate()
	{
		isActive = false;
		deleteZone.SetActive (false);
		Debug.Log ("EditorManager IS NOT active");
	}
	
>>>>>>> master2
	public void Activate()
	{
		windowForEdit = true;
		windowForView = false;
		isActive = true;
		deleteZone.SetActive(true);
		Debug.Log ("Edit mode (isActive = true)");
	}
	
	public void ActivateView()
	{
		windowForEdit = false;
		windowForView = true;
		isActive = false;
		Debug.Log ("View mode (isActive = false)");
	}
	
	public void CloseAndSave()
	{
		StartCoroutine( CloseAndSaveCoroutine() );
	}

	[RPC]
	public void Close()
	{
		networkView.RPC("Close", RPCMode.Others);
		Debug.Log("You choose close option");
		//UI.buttonEditAlreadyIs = false;
		CleanTheRoom ();
	}
<<<<<<< HEAD

=======
/*
	public void OpenFloorPlan()
	{
		CleanTheRoomFloorPlan ();
	}
*/	
>>>>>>> master2
	private IEnumerator CloseAndSaveCoroutine()
	{
		Debug.Log("You choose close&save option");
		yield return StartCoroutine( ListingManager.Instance.SaveListings() );
		CleanTheRoom ();
	}

	public IEnumerator ChangeFloorPlan (int nowIndex)
	{
<<<<<<< HEAD
		nowIndex--;
=======
>>>>>>> master2
		foreach (GameObject obj in list_points)
		{
			Destroy(obj);
		}
		list_points.Clear ();
		string myName = DataBaseCommunicator.Instance.nameFP[nowIndex];	//get file name of FloorPlan
		StorageManager.Instance.LoadFloorPlanImage(myName);			//Load Image to FloorPlan
		yield return null;

		foreach (FloorPlanPoint myVP in EditorManager.instance.floorPlanData[nowIndex])		//set vantagepoints on selected floorplan
		{
			VantagePointOnFP = (GameObject)Instantiate(UI.Instance.vantagePointOnFloorPlan, Vector3.zero, lastFloorPlan.transform.rotation);
			list_points.Add(VantagePointOnFP);
			FloorPlan currentPoint = VantagePointOnFP.GetComponent<FloorPlan>();
			currentPoint.SetData(myVP);
			currentPoint.SetNameText(myVP.linkedVP.Name);
			if(myVP.linkedVP.Id == ListingManager.Instance.currentVantagePoint.Id)
			{
				currentPoint.SetSprite(UI.Instance.current);
			}
			else
			{
				currentPoint.SetSprite(UI.Instance.normal);
			}
			VantagePointOnFP.transform.parent = lastFloorPlan.transform;
			float posFx = -0.5f;
			float posFy = 0.5f;
			float toSetX = myVP.x;
			float toSetY = myVP.y;
			posFx = posFx + toSetX;
			posFy = posFy - toSetY;
			VantagePointOnFP.transform.localPosition = new Vector3(posFx,posFy, transform.localPosition.z - 0.025f);
		}
	}


	public IEnumerator SetFloorPlan(int nowIndex)//set FloorPlan on Scene
	{
<<<<<<< HEAD
		nowIndex--;
=======
>>>>>>> master2
		//Debug.Log ("Index to load " + nowIndex);
		string myName = DataBaseCommunicator.Instance.nameFP[nowIndex];	//get file name of FloorPlan
		//Debug.Log ("File Name: " + myName);
		StorageManager.Instance.LoadFloorPlanImage(myName);			//Load Image to FloorPlan
		ui.transform.rotation = Quaternion.Euler (0, 0, 0);
		lastFloorPlan = (GameObject)Instantiate (StorageManager.Instance.floorPlanPrefab, new Vector3(0,0,0) ,Quaternion.identity);//Load FloorPlane
		lastFloorPlan.transform.position = new Vector3 (0, 0.25f, 3);
		lastFloorPlan.transform.localScale = new Vector3(2 ,2 ,2);
		lastFloorPlan.transform.parent = ui.transform;//transfer object to UI
		foreach (FloorPlanPoint myVP in EditorManager.instance.floorPlanData[nowIndex])		//set vantagepoints on selected floorplan
		{
			//Debug.Log("myVP LINKEDVP on FloorPlan " +myVP.linkedVP.Id);
			VantagePointOnFP = (GameObject)Instantiate(UI.Instance.vantagePointOnFloorPlan, Vector3.zero, lastFloorPlan.transform.rotation);
			list_points.Add(VantagePointOnFP);
			FloorPlan currentPoint = VantagePointOnFP.GetComponent<FloorPlan>();
			currentPoint.SetData(myVP);
			currentPoint.SetNameText(myVP.linkedVP.Name);
			if(myVP.linkedVP.Id == ListingManager.Instance.currentVantagePoint.Id)
			{
				currentPoint.SetSprite(UI.Instance.current);
			}
			else
			{
				currentPoint.SetSprite(UI.Instance.normal);
			}
			VantagePointOnFP.transform.parent = lastFloorPlan.transform;
			float posFx = -0.5f;
			float posFy = 0.5f;
			float toSetX = myVP.x;
			float toSetY = myVP.y;
			posFx = posFx + toSetX;
			posFy = posFy - toSetY;
			VantagePointOnFP.transform.localPosition = new Vector3(posFx,posFy, transform.localPosition.z - 0.025f);
		}
		//rotate UI so it looks the same place as camera
		ui.transform.eulerAngles = new Vector3(0,Camera.main.transform.parent.transform.eulerAngles.y,0);
		yield return null;
	}

	private void CleanTheRoom()
	{
		Debug.Log("Cleaning");
		Destroy(EditorManager.Instance.lastFloorPlan);
		UI.Instance.isSomeWindowOpen = false;
		ListingManager.Instance.ClearHotSpotsList();
		RenderManager.Instance.ClearRendersList();
		RenderManager.Instance.SetRendersToDefaultTexture();	//set black sphere
<<<<<<< HEAD
		isFloorPlanOnScene = false;
		windowForEdit = false;									//for QuestionBoxEdit (ESC)
		windowForView = false;									//for QuestionBoxView (ESC)
		//Deactivate ();
		floorPlanData.Clear();
=======
		windowForEdit = false;									//for QuestionBoxEdit (ESC)
		windowForView = false;									//for QuestionBoxView (ESC)
		//Deactivate ();
		EditorManager.Instance.floorPlanData.Clear();
>>>>>>> master2
		DataBaseCommunicator.Instance.urlFP.Clear ();
		DataBaseCommunicator.Instance.nameFP.Clear ();
		DataBaseCommunicator.Instance.idHotSpot.Clear ();
		ListingManager.Instance.hotSpotsList.Clear ();
		UI.Instance.ShowListingMenu();						//back to menu
<<<<<<< HEAD
		UI.Instance.buttonEditMode.SetActive (true);
=======
>>>>>>> master2
	}
}
