﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

/// <summary>
/// Manage device storage: saving and loading images, serialize and deserialize specified objects. 
/// </summary>
public class StorageManager : MonoBehaviour {
	
	public GameObject floorPlanPrefab;

	private Texture2D floorPlanLoad = null;

	private static StorageManager instance;
	[SerializeField]
	private string imagesDirPath;
	private string imagesDirPathFloorPlan;
	[SerializeField]
	private string savesDirPath;
	private string dataPath;
	
	public void Awake()
	{
		if(instance == null)
			instance = this;
		#if UNITY_EDITOR
		dataPath = Application.dataPath;
		#endif
		#if UNITY_ANDROID && !UNITY_EDITOR
		dataPath = "/storage/sdcard0/vrero";
		#endif
		if(!Directory.Exists(dataPath))
			Directory.CreateDirectory(dataPath);

		imagesDirPathFloorPlan = dataPath + "/" + "FloorPlans";//folder
		imagesDirPath = dataPath + "/" + imagesDirPath;
		savesDirPath = dataPath + "/" + savesDirPath;
		
		if(!Directory.Exists(imagesDirPath))
			Directory.CreateDirectory(imagesDirPath);

		if(!Directory.Exists(imagesDirPathFloorPlan))
			Directory.CreateDirectory(imagesDirPathFloorPlan);
		
		if(!Directory.Exists(savesDirPath))
			Directory.CreateDirectory(savesDirPath);
	}
	
	public static StorageManager Instance
	{
		get { return instance; }
	}

	public string[] ReadListingsMetafile()
	{
		string data = File.ReadAllText("/storage/sdcard0/vrero/listings.meta");
		string listingsNotSplited = data.Split(new char[]{'&'})[0];
		Debug.Log ("ListingsNotSplited (vr app): " + listingsNotSplited);
		string[] listings = listingsNotSplited.Split(new char[]{'-'});
		return listings;
	}
	
	public string ReadAgentIdFromMeta()
	{
		Debug.Log ("load");
		string data = File.ReadAllText("/storage/sdcard0/vrero/listings.meta");
		string agentId = data.Split(new char[]{'&'})[1];
		Debug.Log ("Agent Id " + agentId);
		DataBaseCommunicator.Instance.agentId = int.Parse(agentId);

		return agentId;
	}

	public Texture2D ReadTexture2DFromMemory(string name)
	{
		/*
		byte[] imageBytes = File.ReadAllBytes(path);
		Texture2D newTexture2D = new Texture2D(1, 1);
		newTexture2D.LoadImage(imageBytes);
		return newTexture2D;
		*/

//		Debug.Log ("loading from disk");
		string path = imagesDirPath + "/" + name;
		WWW www = new WWW ("file://" + path);

		while (!www.isDone) 
		{
			//Debug.Log ("Loaded: " + www.progress + "%");
		}
		return www.textureNonReadable;
	}

	public void SaveBytesAsImage(byte[] bytes, string name)
	{
		string path = imagesDirPath + "/" + name;
		File.WriteAllBytes(path, bytes);
	}
	/*
	public void SaveFloorPlanImage(byte[] bytes, string name)//TODO: List for images and load when choose
	{
		string path = imagesDirPathFloorPlan + "/" + name;
		File.WriteAllBytes(path, bytes);
		byte[] data = File.ReadAllBytes(path);
		Texture2D myFP = new Texture2D(1, 1);
		myFP.LoadImage(data);
		Renderer floorPlanRenderer = floorPlanPrefab.renderer;
		floorPlanRenderer.material.mainTexture = myFP;
	}
	*/
	public void SaveFloorPlanImage (byte[] bytes, string name)
	{
		string path = imagesDirPathFloorPlan + "/" + name;
		File.WriteAllBytes(path, bytes);
	}

	public void LoadFloorPlanImage (string name)
	{
		string path = imagesDirPathFloorPlan + "/" + name;




		WWW www = new WWW ("file://" + path);
		
		while (!www.isDone) 
		{
			//Debug.Log ("Loaded: " + www.progress + "%");
		}



		//byte[] data = File.ReadAllBytes(path);
		Texture2D myFP = new Texture2D(1, 1);
		//myFP.LoadImage(data);
		myFP = www.textureNonReadable;
		Renderer floorPlanRenderer = floorPlanPrefab.renderer;
		floorPlanRenderer.material.mainTexture = myFP;
	}

	public string[] FilesInDirectory(string dirPath, string filesType)
	{
		dirPath = dataPath + "/" + dirPath;
		string pattern = "*." + filesType;
		string[] files = Directory.GetFiles(dirPath, pattern);
		return files;
	}
	
	public void SaveObject<T>(T objectToSerialize, string name)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		formatter.Binder = new VersionDeserializationBinder();
		string path = savesDirPath + "/" + name + ".bin";
		FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
		formatter.Serialize(stream, objectToSerialize);
		stream.Close();
	}
	
	public T LoadObject<T>(string pathWithExtension)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		formatter.Binder = new VersionDeserializationBinder();
		FileStream stream = new FileStream(pathWithExtension, FileMode.Open, FileAccess.Read, FileShare.Read);
		T restoredObject = (T)formatter.Deserialize(stream);
		stream.Close();
		return restoredObject;
	}
}
	public sealed class VersionDeserializationBinder : SerializationBinder
	{	
		public override System.Type BindToType(string assemblyName, string typeName)
		{
			if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
			{
				System.Type typeToDeserialize = null;
				assemblyName = Assembly.GetExecutingAssembly().FullName;
				typeToDeserialize = System.Type.GetType(System.String.Format("{0}, {1}", typeName, assemblyName));
				return typeToDeserialize;
			}
			return null;
		}
	}
