﻿using UnityEngine;
<<<<<<< HEAD
using System.Collections;
using System;

public class NetworkManager : MonoBehaviour {

	public string serverName;

    private const string MASTER_SERVER_IP = "212.127.65.100";
    private static NetworkManager instance;
    private bool isAnyClientConnect;

    public static NetworkManager Instance
    {
        get { return instance;  }
    }

    public bool IsAnyClientConnect
    {
        get { return isAnyClientConnect;  }
    }

    void Awake()
    {
        if(instance == null)
            instance = this;

        isAnyClientConnect = false;

        MasterServer.ipAddress = MASTER_SERVER_IP;
        MasterServer.port = 23466;
        Network.natFacilitatorIP = MASTER_SERVER_IP;
        Network.natFacilitatorPort = 50005;
=======
using System.Collections;
using System;

public class NetworkManager : MonoBehaviour {

	public string serverName;

    private const string MASTER_SERVER_IP = "212.127.65.100";
    private static NetworkManager instance;
    private bool isAnyClientConnect;

    public static NetworkManager Instance
    {
        get { return instance;  }
    }

    public bool IsAnyClientConnect
    {
        get { return isAnyClientConnect;  }
    }

    void Awake()
    {
        if(instance == null)
            instance = this;

        isAnyClientConnect = false;

        MasterServer.ipAddress = MASTER_SERVER_IP;
        MasterServer.port = 23466;
        Network.natFacilitatorIP = MASTER_SERVER_IP;
        Network.natFacilitatorPort = 50005;
>>>>>>> master2
    }
	/*
	void Update()
	{
		if (isAnyClientConnect) 
		{
			int currentLisingId = ListingManager.Instance.currentListing.Id;
			int currentVantagePointId = ListingManager.Instance.currentVantagePoint.Id;

			networkView.RPC("`UpdateVantagePoint", RPCMode.Others, currentLisingId, currentVantagePointId);
		}
	}
<<<<<<< HEAD
	*/

    void OnPlayerConnected()
    {
        isAnyClientConnect = true;
=======
	*/

    void OnPlayerConnected()
    {
        isAnyClientConnect = true;
>>>>>>> master2
        Debug.Log(">>>New client connect<<<");

		SendRemoteListingsInfo ();
		/*
		if(!UI.Instance.isListingMenuOpen)
		{
			int listingId = ListingManager.Instance.currentListing.Id;
			int vantagePointId = ListingManager.Instance.currentVantagePoint.Id;
			networkView.RPC ("LoadPresentationRemotly", RPCMode.Others, listingId, vantagePointId);
		}
<<<<<<< HEAD
		*/
    }

    void OnPlayerDisconnected()
    {
        if (Network.connections.Length == 0)
            isAnyClientConnect = false;
        Debug.Log(">>>Client disconnect<<<");
    }

    public void CreateServer()//, Action onCreatedServer)
    {
        Network.InitializeServer(2, 25002, true);
        MasterServer.RegisterHost(serverName, serverName);
		Debug.Log ("### CREATE SERVER with name: " + serverName + " ###");
        
        //onCreatedServer();
=======
		*/
    }

    void OnPlayerDisconnected()
    {
        if (Network.connections.Length == 0)
            isAnyClientConnect = false;
        Debug.Log(">>>Client disconnect<<<");
    }

    public void CreateServer()//, Action onCreatedServer)
    {
        Network.InitializeServer(2, 25002, true);
        MasterServer.RegisterHost(serverName, serverName);
		Debug.Log ("### CREATE SERVER with name: " + serverName + " ###");
        
        //onCreatedServer();
>>>>>>> master2
    }
	/*
	[RPC]
	public void UpdateVantagePoint(int listingId, int vantagePointId)
	{
		int currentListingId = ListingManager.Instance.currentListing.Id;
		int currentVantagePointId = ListingManager.Instance.currentVantagePoint.Id;
		
		if (currentListingId != listingId) 
		{
			Debug.Log (">>> UpdateVantagePoint <<< LISTINGS not equals");
			Main.Instance.LoadPresentationWithChoosenVantagePoint (listingId, vantagePointId);
		}
		else if(currentVantagePointId != vantagePointId)
		{
			Debug.Log (">>> UpdateVantagePoint <<< VANTAGE not equals");
			Listing newListing = ListingManager.Instance.listingsDictionary [listingId];
			VantagePoint newVantagePoint = newListing.VantagePoints [vantagePointId];
			
			newVantagePoint.GoTo ();
		}
	}
*/
	public void LoadPresentationInClient(int listingId)
	{
		networkView.RPC ("LoadPresentationRemotly", RPCMode.Others, listingId);
	}

	public void LoadVantagePointInClient(int vpId)
	{
		networkView.RPC ("LoadVantagePoint", RPCMode.Others, vpId);
	}

	[RPC]
	public void LoadPresentationRemotly(int listingId)
	{
		Main.Instance.LoadPresentation (listingId);
	}

	[RPC]
	public void LoadVantagePoint(int vantagePointId)
	{
		Listing newListing = ListingManager.Instance.listingsDictionary [ListingManager.Instance.currentListing.Id];
		VantagePoint newVantagePoint = newListing.VantagePoints [vantagePointId];
		
		newVantagePoint.GoTo ();
	}
	
	[RPC]
	public void GetRemoteListingsInfo(string listingsNotSplited)
	{

		string[] listings = listingsNotSplited.Split(new char[]{'-'});

		ListingManager.Instance.AddListingsToList(listings);
	}

	public void SendRemoteListingsInfo()
	{
		string listings = ListingManager.listingsToViewIds [0];
		for (int i = 1; i < ListingManager.listingsToViewIds.Count; i++) 
		{
			listings += "-" + ListingManager.listingsToViewIds[i];
		}

		Debug.Log("#### Sended Listings to Slave: ####" + listings);

		networkView.RPC ("GetRemoteListingsInfo", RPCMode.Others, listings);
	}

}
