﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Manage changing of the texture in material of left and right Sphere.
/// </summary>
public class RenderManager : MonoBehaviour {

	private static RenderManager instance;

	public Texture2D defaultTexture;

	public Dictionary<string, Texture> rendersInCurrentListing = new Dictionary<string, Texture>();

	public List<Texture> tempListingCovers = new List<Texture>();

	[SerializeField]
	private Material leftSphereMaterial;
	[SerializeField]
	private Material rightSphereMaterial;

	public void Awake()
	{
		if(instance == null)
			instance = this;

		defaultTexture = Texture2D.blackTexture;
	}

	void Start()
	{
		SetRendersToDefaultTexture ();
	}

	public static RenderManager Instance
	{
		get { return instance; }
	}

	public void AddRendersToList(Image leftImage, Image rightImage)
	{
//		Debug.Log (">>> AddRendersToList(left: " + leftImage.TextureName + ", right: " + rightImage.TextureName + " <<<");

		string rightTextureName = rightImage.TextureName;
		string leftTextureName = leftImage.TextureName;
//		Debug.Log (">>>load left image<<<"+leftImage.TextureName);
//		Debug.Log (">>>load right image<<<"+rightImage.TextureName);
		Texture2D leftTexture = StorageManager.Instance.ReadTexture2DFromMemory(leftImage.TextureName);
		Texture2D rightTexture = StorageManager.Instance.ReadTexture2DFromMemory(rightImage.TextureName);

		if (leftTextureName.Equals (rightTextureName)) 
		{
			rendersInCurrentListing.Add (leftTextureName, leftTexture);
		} 
		else
		{
			rendersInCurrentListing.Add (rightTextureName, rightTexture);
			rendersInCurrentListing.Add (leftTextureName, leftTexture);
		}

	}

	public void ClearRendersList()
	{
		foreach(Texture texture in rendersInCurrentListing.Values)
		{
			DestroyImmediate(texture);
			Resources.UnloadAsset(texture);
		}

		rendersInCurrentListing.Clear();
		Resources.UnloadUnusedAssets ();
	}

	public void SetRendersToSpheres(string rightTextureName, string leftTextureName)
	{
		if (AreReferencesSet()) 
		{
//			Debug.Log ("References set");

			if (leftTextureName.Equals (rightTextureName))
			{
//				Debug.Log ("Right and Left equals. Null reference: " + (rendersInCurrentListing[leftTextureName] == null));

				rightSphereMaterial.mainTexture = rendersInCurrentListing[leftTextureName];
				leftSphereMaterial.mainTexture = rendersInCurrentListing[leftTextureName];

//				Debug.Log ("Textures mono set");

			}
			else
			{
//				Debug.Log ("Right and Left not equals");

				rightSphereMaterial.mainTexture = rendersInCurrentListing[rightTextureName];
				leftSphereMaterial.mainTexture = rendersInCurrentListing[leftTextureName];

//				Debug.Log ("Textures stereo set");
			}
		}
		else
		{
//			Debug.Log("References to Materials are not set!");
			//throw new System.NullReferenceException("References to Materials are not set!");
		}
	}

	public void SetRendersToDefaultTexture()
	{
		leftSphereMaterial.mainTexture = defaultTexture;
		rightSphereMaterial.mainTexture = defaultTexture;
	}

	public void SetRendersToBlack()
	{
		leftSphereMaterial.mainTexture = Texture2D.blackTexture;
		rightSphereMaterial.mainTexture = Texture2D.blackTexture;
	}
	                             

	private bool AreReferencesSet()
	{
		bool status = ((leftSphereMaterial != null) && (rightSphereMaterial != null));
		return status;
	}
}
