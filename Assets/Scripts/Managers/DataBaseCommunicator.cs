﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Http;
/// <summary>
/// Manage connection and communication with Data Base by REST API.
/// </summary>
public class DataBaseCommunicator : MonoBehaviour {

	public List <int> idHotSpot = new List <int> ();
	public List <string> urlFP = new List <string> ();
	public List <string> nameFP = new List <string> ();
	public int maxIndexOfFloorPlan;
<<<<<<< HEAD
	public int currentListingIndex = 0;
=======

>>>>>>> master2
	public delegate void OnSynchorinizedDelegate(Dictionary<int, Listing> newListings);
	public event OnSynchorinizedDelegate OnSynchronized; 
	public Dictionary<int, Listing> listingsDictionary;
	public string urlFloorPlan;
	public string token;
	public int agentId = 2;
	public int listingId;//TODO: take it to tab[] or list

	private static DataBaseCommunicator instance;
	private bool isOnlyOneListing = false;
	private bool rightImageEmpty = false;
	private bool isOnline;
	private int indexFloorPlan = 0;
	public JSONObject json;//private
	private JSONObject listingJSON;
	public JSONObject vantagePointJSON;//private

	#region CONSTS
	//URL
	private const string TEST_NET_URL= "http://www.example.com";
	public const string SERVER_URL = "http://212.127.65.100";   //   "http://vrglobal.com";   //   "http://212.127.65.100";
	public const string DATABASE_APPLICATION_PORT = ":3000";
	 
	//PATH
	//private const string LISTINGS_PATH = "/listing/vantage_point/agent/";
	private const string LISTINGS_PATH = "/listing/public";
	//private const string LISTINGS_PATH = "/listing/agent/";
	private const string FLOORPLAN_PATH = "listing_floorplan_layouts"; 	//for test only
	private const string FLOORPLAN_VANTAGE_POINT = "vantage_points";   	//for test only
	private const string FLOORPLAN_URL_PATH = "url_path";

	//IDENTIFY
	private const string AUTH_PATH = "/auth";
	private const string ID_KEY = "id";
	private const string TOKEN_KEY = "auth_token";
	private const string AGENT_ID_KEY = "agent_id";
	private const string VANTAGE_POINT_KEY = "vantage_points";
	private const string RENDERS_KEY = "vantage_point_renders";
	private const string HOTSPOT_KEY = "vantage_point_hotspots";
	private const string DATE_KEY = "created_at";
	private const string STARTING_VP_KEY = "initial_vantage_point_id";
	private const string NAME_KEY = "name";
	private const string LISTING_IMAGES_KEY = "listing_pictures";
	private const string AGENT_KEY = "agent";
	private const string LISTING_STATUS_KEY = "listing_status";
	
	//LOGIN
	private const string USER_LOGIN = "agent@vrero.com";//"l.adamczyk@vrero.com";//test@test.com
	private const string USER_PASS = "agent";//adamczyk//kowalski//test

	#endregion
	
	void Awake()
	{
		if(instance == null)
			instance = this;
	}

	public static DataBaseCommunicator Instance
	{
		get { return instance; }
	}

	public bool IsOnline
	{
		get { return isOnline; }
	}

	public string Token
	{
		get { return token; }
	}

	public JSONObject jSonHS
	{
		get { return vantagePointJSON; }
	}

	public void StartSynchronize(Dictionary<int, Listing> mainListingsDictionary)
	{
		listingsDictionary = mainListingsDictionary;
		StartCoroutine(SynchronizeCoroutine());
	}
	
	private IEnumerator SynchronizeCoroutine()
	{
		yield return StartCoroutine(CheckIfOnlineCoroutine());
		//yield return StartCoroutine(TryToLogin());
		OnSynchronized(listingsDictionary);
	}
	
	private IEnumerator CheckIfOnlineCoroutine()
	{
		WWW www = new WWW(TEST_NET_URL);
		yield return www;
		if(www.text.Length > 0)
		{
			isOnline = true;
			yield return StartCoroutine(TryToLogin());
			//yield return StartCoroutine(GetListingsJSON());
			//yield return StartCoroutine(GetListings());
		}
		else
		{
			isOnline = false;
			UI.Instance.SetOfflineMessage();				
			Debug.Log ("Device is: offline");
		}
	}

	private IEnumerator TryToLogin()
	{
		HttpRequest postRequest = new HttpRequest(HttpMethod.POST, SERVER_URL + DATABASE_APPLICATION_PORT + AUTH_PATH);
		yield return null;
		postRequest.AddField("email", USER_LOGIN);
		postRequest.AddField("password", USER_PASS);
		string feedback = postRequest.Send();
		Debug.Log ("feedback: " + feedback);
		JSONObject agenttJson = new JSONObject(feedback); 
	//agentId = (int)agenttJson.GetField(AGENT_ID_KEY).n;
		token = agenttJson.GetField(TOKEN_KEY).str;
		Debug.Log("agentId: " + agentId + " token: " + token);
		if(token==null)
		{
			Debug.Log("Token is empty");
		}
		else
		{
			yield return StartCoroutine (GetListingsJSON());
			yield return StartCoroutine (GetListings());
<<<<<<< HEAD
			//yield return StartCoroutine (TEST ());
		}
	}

	public void RemoveHSFromList (HotSpotData data)
	{
		StartCoroutine (RemoveMyHS (data));
	}

	//public void AddHSToList (HotSpotData data)
	//{
	//	StartCoroutine (AddMyHS (HotSpotData data));
	//}

	private IEnumerator RemoveMyHS(HotSpotData data)
	{
		foreach (List <List<HotSpotData>> list_test  in ListingManager.Instance.allHotSpotsList[currentListingIndex])
		{
			foreach (List <HotSpotData> vp_test in list_test)
			{
				foreach (HotSpotData test in vp_test)
				{
					Debug.Log("HOTSPOTDATA IN LIST " + test.id);
					if (test.id == data.id)
					{
						vp_test.Remove(data);
					}
				}
			}
		}
		yield return null;
	}
	private IEnumerator TEST ()
	{
		foreach (List <List<HotSpotData>> list_test  in ListingManager.Instance.allHotSpotsList[0])
		{
			Debug.Log("listing [0]");
			foreach (List <HotSpotData> vp_test in list_test)
			{
				//Debug.Log ("vp");
				foreach (HotSpotData test in vp_test)
				{
					Debug.Log("HOTSPOTDATA IN LIST " + test.id);
				}
			}
		}
		foreach (List <List<HotSpotData>> list_test  in ListingManager.Instance.allHotSpotsList[1])
		{
			Debug.Log("listing [1]");
			foreach (List <HotSpotData> vp_test in list_test)
			{
//				Debug.Log ("vp");
				foreach (HotSpotData test in vp_test)
				{
					Debug.Log("HOTSPOTDATA IN LIST " + test.id);
				}
			}
		}
		yield return null;
	}
	
=======
		}
	}
>>>>>>> master2

	private IEnumerator GetListingsJSON()
	{
		HttpRequest getRequest = new HttpRequest (HttpMethod.GET, SERVER_URL + DATABASE_APPLICATION_PORT + LISTINGS_PATH);//agentId
		yield return null;
		string jsonString = getRequest.Send();
		Debug.Log ("JSON: "+jsonString);
		json = new JSONObject(jsonString);
	}

	private IEnumerator GetListings()
	{
		int listingsNumber = json.Count;
		int list_nr = 0;
		for(int i = 0; i < listingsNumber; i++)
		{
			listingJSON = json[i];
			vantagePointJSON = listingJSON.GetField(VANTAGE_POINT_KEY);
			listingId = (int)listingJSON.GetField(ID_KEY).n;
			if(!listingsDictionary.ContainsKey(listingId))
			{
				Listing newListing = new Listing();
				newListing.Set(listingId, new Dictionary<int, VantagePoint>());
				listingsDictionary.Add(listingId, newListing);
				Debug.Log("Added listing " + listingId + " to dictionary.");
				string listingCoverPath = "";
				string listingCoverFileName = "";

				try
				{
					listingCoverPath = listingJSON.GetField(LISTING_IMAGES_KEY)[0].GetField("url_path").str;
					listingCoverPath = SERVER_URL + DATABASE_APPLICATION_PORT + listingCoverPath;
					listingCoverFileName = (listingId.ToString() + "_cover.vrero");
				}
				catch
				{
					Debug.LogError(">>> ERROR <<< listingCoverPath key / value not found!");
				}

				yield return StartCoroutine( DownloadTexture(listingCoverPath, listingCoverFileName));
			}
			SetListingInfo(listingId);
			int vantagePointsNumber = vantagePointJSON.Count;//2
			for(int j = 0; j < vantagePointsNumber; j++)//ok
			{
				UI.Instance.SetSyncCounter(i+1, listingsNumber, j+1, vantagePointsNumber);
				yield return StartCoroutine(GetVantagePoint (listingId, j));
			}
			Debug.Log (">>>>>>> LOAD HOTSPOT <<<<<< FOR LISTING "+i);
			for(int j = 0; j < vantagePointsNumber; j++)//ok
			{
<<<<<<< HEAD
				Debug.Log ("Vantage Point >>>>>>> "+j+" <<<<<<");
				yield return StartCoroutine (GetHotSpots (listingId, j, i));
			}
		}
	}
	 
=======
				Debug.Log (">>>>>>> "+j+" <<<<<<");
				yield return StartCoroutine (GetHotSpots (listingId, j));
			}
		}
	}

>>>>>>> master2
	private void SetListingInfo(int id)
	{
		Listing listing = listingsDictionary[id];
		Agent agent = new Agent ();
		JSONObject agentJSON = listingJSON.GetField (AGENT_KEY);
		try
		{
			string agentName = agentJSON.GetField ("first_name").str;
			string agentSurname = agentJSON.GetField ("last_name").str;
			string agentEmail = agentJSON.GetField ("email").str;
			string agentPhotoPath = "";
			try
			{
				agentPhotoPath = agentJSON.GetField("photo").str;
			}
			catch
			{
				Debug.LogError(">>> ERROR <<< Loadin Agent Image");
			}
			//Debug.Log (">>> SETTINH GENT INFO <<< agent photo path: " + agentPhotoPath);
			agent.SetInfo (agentName, agentSurname, agentEmail, agentPhotoPath);
		}
		catch
		{
			Debug.LogError(">>> ERROR <<< Loading Agnet data");
		}
		/*
		if (!agent.imgPath.Equals ("")) 
		{
			StartCoroutine( DownloadTexture(agent.imgPath, "agent_photo_" + id) );
		}*/
		listing.agent = agent;
		listing.startingVantagePointId = (int)listingJSON.GetField(STARTING_VP_KEY).n;
		listing.name = listingJSON.GetField(NAME_KEY).str;
		listing.description = listingJSON.GetField("description").str;
		listing.price = "$" + listingJSON.GetField("price").str;
		listing.area = (int)listingJSON.GetField("area").n;
		listing.zipCode = listingJSON.GetField("zip_code").str;
		listing.street = listingJSON.GetField("street").str;
		listing.country = listingJSON.GetField("country").GetField(NAME_KEY).str;
		listing.staus = listingJSON.GetField (LISTING_STATUS_KEY).GetField (NAME_KEY).str;
	}

	private IEnumerator GetVantagePoint(int listingId, int index)
	{
		int vantagePointId = (int)vantagePointJSON[index].GetField(ID_KEY).n;
		string vantagePointName = vantagePointJSON[index].GetField(NAME_KEY).str;
		string rightImgPath = "";
		string leftImgPath = "";
		string leftImgUrl = "";
		string rightImgUrl = "";
		string rightImgName = "";
		string leftImgName = "";
		string rendersCreatedDate = "";
		try
		{
			leftImgPath = vantagePointJSON[index].GetField(RENDERS_KEY).GetField("left_url_path").str;
			rightImgPath = vantagePointJSON[index].GetField(RENDERS_KEY).GetField("right_url_path").str;
		}
		catch
		{
			Debug.LogError("Left/Right ImgPath url_path key not found !");
		}
		if (rightImgPath == "" || rightImgPath == null)
		{
			rightImgPath = leftImgPath;
			rightImageEmpty = true;
		}
		else
		{
			rightImageEmpty = false;
		}
		if (rightImageEmpty == true)
		{
			leftImgUrl =  SERVER_URL + DATABASE_APPLICATION_PORT + leftImgPath;
			rightImgUrl = leftImgUrl;
		}
		else
		{
			leftImgUrl =  SERVER_URL + DATABASE_APPLICATION_PORT + leftImgPath;
			rightImgUrl = SERVER_URL + DATABASE_APPLICATION_PORT + rightImgPath;
		}
		rightImgName = listingId + "_" + vantagePointId + "_R.vrero";
		leftImgName = listingId + "_" + vantagePointId + "_L.vrero";
		try
		{
			rendersCreatedDate = vantagePointJSON[index].GetField(RENDERS_KEY).GetField(DATE_KEY).str;
		}
		catch
		{
			Debug.LogError("Not found : vantage_point_renders");
		}

		if(listingsDictionary[listingId].ContainsVantagePoint(vantagePointId))
		{
//			Debug.Log(">>>Already contain VantagePoint ID<<< "+vantagePointId);
//			Debug.Log ("In listing: " + index);
			VantagePoint tempVantagePoint = listingsDictionary[listingId].VantagePoints[vantagePointId];
			Sphere tempSphere = tempVantagePoint.Sphere;
			//Debug.Log("Old json hotspots (in json): "+vantagePointJSON.GetField("vantage_point_hotspots"));
			if(vantagePointJSON[index].GetField("vantage_point_hotspots").Count == 0)
			{
//				Debug.Log("NO MORE HOTSPOTS IN DATABASE TO LOAD");
			}
			else
			{
				JSONObject hotSpotJson = vantagePointJSON[index].GetField("vantage_point_hotspots");
//yield return StartCoroutine(tempVantagePoint.LoadHotSpotFromDataBase(hotSpotJson));
			}
			//Load HotSpots From DataBase (All HotSpots[VP[id]] belong to VP[id])
			//Debug.Log("Current VantagePoint: " + vantagePointJSON[index]/*.GetField("vantage_point_hotspot").Count*/);
			//Debug.Log ("Number of Hotspots in current VantagePoint: " + vantagePointJSON[index].GetField("vantage_point_hotspots").Count);
			if(tempSphere.CheckIfObsolete(rendersCreatedDate))
			{
				if(rightImageEmpty == true)
				{
					yield return StartCoroutine(DownloadTexture(leftImgUrl, leftImgName));
					tempSphere.LeftImage.Set(leftImgName, rendersCreatedDate);
					tempSphere.RightImage.Set(leftImgName, rendersCreatedDate);
				}
				else
				{
					yield return StartCoroutine(DownloadTexture(leftImgUrl, leftImgName));
					yield return StartCoroutine(DownloadTexture(rightImgUrl, rightImgName));
					tempSphere.LeftImage.Set(leftImgPath, rendersCreatedDate);
					tempSphere.RightImage.Set(rightImgName, rendersCreatedDate);
				}
			}
		}
		else
		{
			Image leftImage = new Image();
			Image rightImage = new Image();
			Debug.Log ("Create new VantagePoint");
			if(rightImageEmpty == true)
			{
				yield return StartCoroutine(DownloadTexture(leftImgUrl, leftImgName));
				leftImage.Set(leftImgName, rendersCreatedDate);
				rightImage.Set(leftImgName, rendersCreatedDate);
			}
			else
			{
				yield return StartCoroutine(DownloadTexture(rightImgUrl, rightImgName));
				yield return StartCoroutine(DownloadTexture(leftImgUrl, leftImgName));
				leftImage.Set(leftImgName, rendersCreatedDate);
				rightImage.Set(rightImgName, rendersCreatedDate);
			}
			Sphere sphere = new Sphere();
			sphere.Set(rightImage, leftImage);
			/*
			foreach (VantagePoint test in listingsDictionary[listingId].VantagePoints.Values)
			{
				//Debug.Log(">>> before load floorplan - test ID VP in dictionary in DataBaseCommunicator: <<<"+test.Id);//2x15
			}
			*/
			VantagePoint currentVantagePoint = new VantagePoint();
			currentVantagePoint.Set(vantagePointId, vantagePointName, listingId, sphere);
			listingsDictionary[listingId].VantagePoints.Add(vantagePointId, currentVantagePoint);

//			if(vantagePointJSON[index].GetField("vantage_point_hotspots").Count == 0)
//			{
//				Debug.Log("NO MORE HOTSPOTS IN DATABASE TO LOAD");
//			}
//			else
//			{
//				JSONObject hotSpotJson = vantagePointJSON[index].GetField("vantage_point_hotspots");
//yield return StartCoroutine(currentVantagePoint.LoadHotSpotFromDataBase(hotSpotJson));
//			}
		}
	}

<<<<<<< HEAD
	private IEnumerator GetHotSpots (int listingId, int VP_index, int listing_index)
	{
		int vantagePointId = (int)vantagePointJSON[VP_index].GetField(ID_KEY).n;
		string vantagePointName = vantagePointJSON[VP_index].GetField(NAME_KEY).str;
=======
	private IEnumerator GetHotSpots (int listingId, int index)
	{
		int vantagePointId = (int)vantagePointJSON[index].GetField(ID_KEY).n;
		string vantagePointName = vantagePointJSON[index].GetField(NAME_KEY).str;
>>>>>>> master2
		string rightImgPath = "";
		string leftImgPath = "";
		string leftImgUrl = "";
		string rightImgUrl = "";
		string rightImgName = "";
		string leftImgName = "";
		string rendersCreatedDate = "";

		if(listingsDictionary[listingId].ContainsVantagePoint(vantagePointId))
		{
			VantagePoint tempVantagePoint = listingsDictionary[listingId].VantagePoints[vantagePointId];
<<<<<<< HEAD
			//EditorManager.Instance.floorPlanData.Add(new List<FloorPlanPoint>());
			//EditorManager.Instance.floorPlanData[0].Add(floorPlan);

			//[1] [2] [3] 
			//[1] Listing
			//[2] VantagePoint
			//[3] HotSpots

			ListingManager.Instance.allHotSpotsList.Add(new List< List <List <HotSpotData>>>());//new list

			ListingManager.Instance.allHotSpotsList[listing_index].Add(new List< List <HotSpotData>>());//for new listing









			//int c = ListingManager.Instance.test_list.Count;
			//int c0 = ListingManager.Instance.test_list[0].Count;
			//int c1 = ListingManager.Instance.test_list[1].Count;

			//Debug.Log ("c = " + c);
			//Debug.Log ("c0 = " + c0);
			//Debug.Log ("c1 = "+ c1);

			//for (int i = 0; i<
			//foreach (string sss in ListingManager.Instance.test_list[0][0])
			//{
			//	Debug.Log("TEXT IN LIST " + sss);
			//}
			//foreach (string sss in ListingManager.Instance.test_list[1][0])
			//{
			//	Debug.Log("TEXT IN LIST " + sss);
			//}


			if(vantagePointJSON[VP_index].GetField("vantage_point_hotspots").Count == 0)
=======
			//Sphere tempSphere = tempVantagePoint.Sphere;
			//Debug.Log("Old json hotspots (in json): "+vantagePointJSON.GetField("vantage_point_hotspots"));
			if(vantagePointJSON[index].GetField("vantage_point_hotspots").Count == 0)
>>>>>>> master2
			{
//				Debug.Log("NO MORE HOTSPOTS IN DATABASE TO LOAD");
			}
			else
			{
<<<<<<< HEAD
				JSONObject hotSpotJson = vantagePointJSON[VP_index].GetField("vantage_point_hotspots");
				yield return StartCoroutine(tempVantagePoint.LoadHotSpotFromDataBase(hotSpotJson, VP_index, listing_index));
			}
			yield return null;
=======
				JSONObject hotSpotJson = vantagePointJSON[index].GetField("vantage_point_hotspots");
				yield return StartCoroutine(tempVantagePoint.LoadHotSpotFromDataBase(hotSpotJson));
			}
>>>>>>> master2
		}
	}

	public void GetNew (int currentID)
	{
		StartCoroutine (GetNewHotSpotId (currentID));
	}

	private IEnumerator GetNewHotSpotId (int currrentID)
	{
		Debug.Log ("Get a new JSON from database");
		HttpRequest getRequest = new HttpRequest (HttpMethod.GET, SERVER_URL + DATABASE_APPLICATION_PORT + LISTINGS_PATH);//agentId
		yield return null;
		string jsonString = getRequest.Send();
		Debug.Log ("JSON: "+jsonString);
		json = new JSONObject(jsonString);
		Debug.Log ("current listing id = " + currrentID);
		int jsonListingId;
<<<<<<< HEAD
		//int currentListingIndex = 0;
=======
		int currentListingIndex = 0;
>>>>>>> master2
		int currentListingId = currrentID;
		for(int i = 0; i < json.Count; i++)//searching for current listing in Json (index)
		{
			jsonListingId = (int)json[i].GetField(ID_KEY).n;
			if (jsonListingId == currentListingId)
			{
				currentListingIndex = i;
			}
		}
		Debug.Log ("current listing index = " + currentListingIndex);
		Debug.Log (">>>FIND<<< Current Listing ID = " + currentListingId + " Current Listing Index (in JSON) = " + currentListingIndex);
		JSONObject listingIndex = json [currentListingIndex];
		JSONObject vantagePointKey = listingIndex.GetField (VANTAGE_POINT_KEY);
		int currentVantagePointId = ListingManager.Instance.currentVantagePoint.Id;
		int vp_id = 0;
		int index = 0;
		for (int i = 0; i < vantagePointKey.Count; i++)
		{
//			Debug.Log("Searching for current vp in json");
			vp_id = (int) vantagePointKey[i].GetField("id").n;
			if (vp_id == currentVantagePointId)
			{
				Debug.Log ("found current vp");
				Debug.Log ("index in JSon = " + i);
				Debug.Log ("vp id = " + vp_id);
				index = i;
			}
		}
		int hotSpotCount = 0;
		JSONObject hotspot_Id;
		hotSpotCount = vantagePointKey [index].GetField ("vantage_point_hotspots").Count;
		hotspot_Id = vantagePointKey [index].GetField ("vantage_point_hotspots");
		Debug.Log ("hotspots ID Json in current vp = " + hotspot_Id);
<<<<<<< HEAD

=======
>>>>>>> master2
		if(hotSpotCount > 0)
		{
			for (int i = 0; i < hotSpotCount; i++)
			{
//				Debug.Log ("hotspot add to new list");
				int id = (int) hotspot_Id[i].GetField("id").n;
				idHotSpot.Add(id);
			}
		}
<<<<<<< HEAD
		//Debug.Log("list of hotspots ID");
		//foreach (int id in idHotSpot)
		//{
		//	Debug.Log("HotSpot id " + id);
		//}
=======
		Debug.Log("list of hotspots ID");
		foreach (int id in idHotSpot)
		{
			Debug.Log("HotSpot id " + id);
		}
>>>>>>> master2
	}

	public void GetFloorPlanPosition(int currentlisting_Id)
	{
		maxIndexOfFloorPlan = 0;
		Debug.Log ("FLOORPLAN");
		Debug.Log("currentListing.Id = "+currentlisting_Id);
		VantagePoint newVantagePoint = new VantagePoint();
		int jsonListingId;
<<<<<<< HEAD
		//int currentListingIndex = 0;
=======
		int currentListingIndex = 0;
>>>>>>> master2
		
		for(int i = 0; i < json.Count; i++)//searching for current listing in Json (index)
		{
			jsonListingId = (int)json[i].GetField(ID_KEY).n;
			
			if (jsonListingId == currentlisting_Id)
			{
				currentListingIndex = i;
			}
		}
		Debug.Log (">>>FIND<<< Current Listing ID = " + currentlisting_Id + " Current Listing Index (in JSON) = " + currentListingIndex);
		JSONObject floorPlanJSON = json[currentListingIndex].GetField(FLOORPLAN_PATH);//get current floorPlans [] 
		for (int i = 0; i<floorPlanJSON.Count; i++) //for each of current floorplans
		{
//for (int k = 0; k<4; k++)
//{
			maxIndexOfFloorPlan++;
			Debug.Log ("For Each FloorPlan");
			Debug.Log ("Loading FloorPlan [index] = " + i);


			float FP_id = (int)floorPlanJSON[i].GetField("id").n;
			string FP_id_convert = FP_id.ToString();									
			JSONObject takeVP = floorPlanJSON[i].GetField(FLOORPLAN_VANTAGE_POINT);//VantagPoints in current floorplan
			urlFloorPlan = SERVER_URL + DATABASE_APPLICATION_PORT + floorPlanJSON[i].GetField(FLOORPLAN_URL_PATH).str;
			urlFP.Add(urlFloorPlan);//add image url to the list
			for (int j = 0; j<takeVP.Count; j++)//VantagePoints in current floorplan
			{
				int idVP;
				float xVP, yVP, zVP;
				idVP = (int)takeVP[j].GetField("id").n;
				xVP = takeVP[j].GetField("x").n;
				yVP = takeVP[j].GetField("y").n;
				Listing mylist = ListingManager.Instance.currentListing;
				VantagePoint linkedVP = mylist.VantagePoints[idVP];
				FloorPlanPoint floorPlan = new FloorPlanPoint(xVP, yVP, 0, linkedVP);
				//EditorManager.Instance.floorPlanData.Add(floorPlan);//add VantagePoints (to List) for current FloorPlan 
				//TODO: list[0].add(list[all]) ?
			EditorManager.Instance.floorPlanData.Add(new List<FloorPlanPoint>());
			EditorManager.Instance.floorPlanData[i].Add(floorPlan);
			}
			//FP_id_convert = "test";//ok TODO: add FP_id_convert to tab[] or list<>
			FP_id_convert += ".vrero";
			nameFP.Add(FP_id_convert);//add file name to the list
			StartCoroutine(GetFloorPlanImage(urlFloorPlan, FP_id_convert));//load floorplan from database and save it to FloorPlans dir
//}
		}
	}

	private IEnumerator DownloadTexture(string imgUrl, string imgName)
	{
<<<<<<< HEAD
		WWW www = new WWW(imgUrl);
		yield return www;
		Texture2D tempTexture = new Texture2D(1, 1);
		www.LoadImageIntoTexture(tempTexture);
		byte[] imgBytes = tempTexture.EncodeToPNG();
		Destroy (tempTexture);
=======
		Debug.Log (imgUrl);
		WWW www = new WWW(imgUrl);
		yield return www;
		Texture2D tempTexture = new Texture2D(1, 1);
		//Texture2D tempTexture = new Texture2D(1, 1, TextureFormat.DXT1, false);
		www.LoadImageIntoTexture(tempTexture);
		//byte[] imgBytes = tempTexture.EncodeToPNG();
		byte[] imgBytes = www.bytes;
		DestroyImmediate (tempTexture);
>>>>>>> master2
		StorageManager.Instance.SaveBytesAsImage(imgBytes, imgName);
		yield return null;
	}

	private IEnumerator GetFloorPlanImage(string imgUrl, string imgName)
	{
		WWW www = new WWW(imgUrl);
		yield return www;
		Texture2D tempTexture = new Texture2D(1, 1);
		www.LoadImageIntoTexture(tempTexture);
		byte[] imgBytes = tempTexture.EncodeToPNG();
<<<<<<< HEAD
		Destroy (tempTexture);
=======
		DestroyImmediate(tempTexture);
>>>>>>> master2
		StorageManager.Instance.SaveFloorPlanImage(imgBytes, imgName);
	}
}
