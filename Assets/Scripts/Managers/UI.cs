﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Create and manage user interface in application.
/// </summary>
public class UI : MonoBehaviour
{
	public GameObject buttonEditMode;
	public GameObject questionBoxEdit;
	public GameObject vantagePointOnFloorPlan;
	public GameObject editOn;			//Button Edit prefab
	public GameObject lastEdit;			//gameObject from Button Edit prefab (destroy it in Main.cs)
	public GameObject floorPlanPrefab;
	public GameObject tailsMenuPreab;
	public GameObject tailPrefab;		//tail for listing
	public GameObject tailPrefab_VP;	//tail in room
	public GameObject questionBoxPrefabEdit;
	public GameObject questionBoxPrefabView;
	public GameObject scrollingMenuPrefab;
	public GameObject crosshair;
<<<<<<< HEAD
=======
	public bool setFloorPlan = false; 
>>>>>>> master2
	public Sprite normal;//VantagePoint default in FloorPlan (to get Sprite-prefab)
	public Sprite select;//VantagePoint selected in FloorPlan (to get Sprite-prefab)
	public Sprite current;
	public Transform ovrCamera;
	public Transform scrollingMenu;
	//public bool switchEditButtonOnOff = false;	//to switch edit BUTTON on/off, if off then set view mode
	public bool isSomeWindowOpen;
	public bool isFloorPlanEdit = false;//for change color, edit, florplan ready to create hotspot
	//public static bool buttonEditAlreadyIs = false;
	public bool isListingMenuOpen;
<<<<<<< HEAD
	public bool clearPoint = false;	
=======
		
>>>>>>> master2
	private static UI instance;
	private ListingManager listingManager;
	private RenderManager renderManager;
			
	[SerializeField]
	public GameObject syncScreen;

	[SerializeField]
	private Text syncText;

		void Awake ()
		{
				if (instance == null)
						instance = this;
				listingManager = ListingManager.Instance;
				renderManager = RenderManager.Instance;
				isListingMenuOpen = false;
				renderManager.SetRendersToDefaultTexture ();
				HideSyncScreen ();
		}

		public static UI Instance 
		{
				get { return instance; }
		}

		public void ShowSyncScreen ()
		{
<<<<<<< HEAD
			renderManager.SetRendersToBlack ();
			crosshair.SetActive (false);
			syncScreen.SetActive (true);
=======
			crosshair.SetActive (false);
			syncScreen.SetActive (true);
			renderManager.SetRendersToBlack ();

>>>>>>> master2
		}

		public void HideSyncScreen ()
		{
			/*try{
			ovrCamera.GetComponent<OVRCameraController> ().freezRotation = false;
			}catch{Debug.LogError("OVRCameraController enabled ERROR");}*/
			syncScreen.SetActive (false);
			crosshair.SetActive (true);
		}

		public void SetLoadingMessage ()
		{
				syncText.text = "Loading";
		}

		public void SetOfflineMessage ()
		{
				syncText.text = "Offline";
		}

		public void SetSyncCounter (int currentNumber, int maxNumber, int currentVPNumber, int maxVPNumber)
		{
				syncText.text = "Synchronizing listings " + currentNumber + "/" + maxNumber + "\n Synchronizing vantagepoints " + currentVPNumber + "/" + maxVPNumber; 
		}

		public void ShowVantagePointMenu (HotSpot postedHotSpot)
		{
				if (!isSomeWindowOpen) 
				{
						isSomeWindowOpen = true;

						Vector3 postion = Vector3.zero;
						Vector3 angles = Vector3.zero;

						TailsMenu<HotSpot> tailsMenu = new TailsMenu<HotSpot> ();

						foreach (VantagePoint vantagePoint in ListingManager.Instance.currentListing.VantagePoints.Values) 
						{
				Debug.Log("Creating tailVP and getting textures");
								Texture texture = renderManager.rendersInCurrentListing [vantagePoint.PreviewImageName];
								tailsMenu.AddTail (postedHotSpot, postedHotSpot.SetLinkedVantagePoint, vantagePoint.Name, texture);
						}

						postedHotSpot.Deactivate ();

						SetPositionInSpace (tailsMenu.Transform);
				}
		}
		//[RPC]
		public void ShowListingMenu ()
		{
			//networkView.RPC ("ShowListingMenu", RPCMode.Others);
			isListingMenuOpen = true;
			RenderManager.Instance.SetRendersToDefaultTexture ();
			//EditorManager.Instance.isEditButtonDelete = false;
			if (!isSomeWindowOpen) 
			{
				isSomeWindowOpen = true;
				ScrollingMenu scrollingMenu = ScrollingMenu.Inst ();
				this.scrollingMenu = scrollingMenu.transform;
				int index = 0;
#if UNITY_EDITOR
		int i=0;
		//for(int j=0;j<5;j++)
		//{
				foreach (Listing listing in ListingManager.Instance.listingsDictionary.Values) 
				{
					string name = listing.name;
					Texture texture = RenderManager.Instance.tempListingCovers [1];
					scrollingMenu.AddTail (Main.Instance.LoadPresentation, name,"10", listing.description, texture,i, listing.agent, listing.staus);
					++i;
				}
		//}
#else
				foreach(string listingId in ListingManager.listingsToViewIds)
				{
					try
					{
						int id = int.Parse(listingId);

						Listing listing = ListingManager.Instance.listingsDictionary[id];
						index = ListingManager.Instance.ListingIndex(id);
						string name = listing.name;
						string price = listing.price;

						Texture texture = StorageManager.Instance.ReadTexture2DFromMemory(listingId + "_cover.vrero");
					scrollingMenu.AddTail (Main.Instance.LoadPresentation, name, price, listing.description, texture, index, listing.agent, listing.staus);
					}
					catch
					{
						Debug.LogError(">>> ERROR <<< Error in displaying listing menu!");
					}
				}
#endif
				scrollingMenu.transform.position = new Vector3 (0, 0, 0);
				scrollingMenu.transform.position = Camera.main.transform.position;
				scrollingMenu.transform.rotation = Quaternion.Euler (0, Camera.main.transform.eulerAngles.y, 0);
				scrollingMenu.transform.SetParent(ovrCamera);
			}
		}

		public void ShowClosingBoxEdit ()
		{
			//EditorManager.Instance.windowForEdit = true;
			//questionBoxEdit = QuestionBoxEdit.Inst (EditorManager.Instance.CloseAndSave,EditorManager.Instance.Close, Cancel, "What do you want to do?");
			//questionBoxEdit.transform.position = new Vector3(0,0,4);
			//UI.instance.transform.rotation = Quaternion.Euler (0, 0, 0);
			//questionBoxEdit.transform.parent = UI.instance.transform;
			//UI.instance.transform.rotation = Camera.main.transform.parent.transform.rotation;
			
		Debug.Log ("click back - floorplanedit = false");
		transform.rotation = Camera.main.transform.parent.transform.rotation;
		//	isFloorPlanEdit = false; 									// when click Back
			StartCoroutine (EditorManager.Instance.SetFloorPlan (EditorManager.Instance.nowIndexOfFloorPlan));
		}

		public void ShowFloorPlan ()
		{
			if(!isListingMenuOpen)
			{
<<<<<<< HEAD
			if (EditorManager.Instance.howManyHSinPoint == 1)
			{
				clearPoint = true;
			}
				Debug.Log("Start floorplan ");
				EditorManager.Instance.isFloorPlanOnScene = true;
=======
				Debug.Log("Start floorplan ");
				setFloorPlan = true;
				
>>>>>>> master2
				if (EditorManager.Instance.IsActive == true)
				{
				//	isFloorPlanEdit = true; 							// when click TouchPad
				StartCoroutine(EditorManager.Instance.SetFloorPlan(EditorManager.Instance.nowIndexOfFloorPlan));
				}
				else
				{
				//isFloorPlanEdit = false;
				StartCoroutine(EditorManager.Instance.SetFloorPlan(EditorManager.Instance.nowIndexOfFloorPlan));
				}
			}
		}

		public void SetPositionInSpace (Transform transformToModyfi)
		{
			transformToModyfi.position = Pointer.Instance.transform.forward * 8;
			transformToModyfi.eulerAngles = Pointer.Instance.transform.eulerAngles;
		}
	
		public void Cancel ()
		{
			Debug.Log ("You choose Cancel");
		}
}
