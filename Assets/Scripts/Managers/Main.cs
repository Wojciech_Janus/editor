using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Main Class of applications. It is using Menagers to communicate with objects. 
/// Main application process start from Run method.
/// </summary>
public class Main : MonoBehaviour {

	private static Main instance;

	#region MANAGERS
	private ListingManager listingManager;
	private DataBaseCommunicator dataBaseCommunicator;
	private StorageManager storageManager;
	private EditorManager editorManager;
<<<<<<< HEAD
	private UI ui;
=======
	private UI ui;
>>>>>>> master2
    private NetworkManager networkManager;
	#endregion MANAGERS

	public static Main Instance
	{
		get { return instance; }
	}

	void Awake()
	{
		if(instance == null)
			instance = this;

		listingManager = ListingManager.Instance;
		dataBaseCommunicator = DataBaseCommunicator.Instance;
		storageManager = StorageManager.Instance;
		ui = UI.Instance;
<<<<<<< HEAD
		editorManager = EditorManager.Instance;
=======
		editorManager = EditorManager.Instance;
>>>>>>> master2
        networkManager = NetworkManager.Instance;
		
		dataBaseCommunicator.OnSynchronized += AfterSynchronized;	
	}
	
	void Start()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		NetworkManager.Instance.serverName = storageManager.ReadAgentIdFromMeta();
		Debug.Log(">>> META FILE <<<");
<<<<<<< HEAD
#endif
		listingManager.PopulateListingsDictionary();

        //string agentId = dataBaseCommunicator.agentId.ToString();
=======
#endif
		listingManager.PopulateListingsDictionary();

        //string agentId = dataBaseCommunicator.agentId.ToString();
>>>>>>> master2
        //networkManager.CreateServer(agentId, StartSynchronize);
		StartSynchronize ();
		ui.ShowSyncScreen();
	}

<<<<<<< HEAD
    void StartSynchronize()
    {
        dataBaseCommunicator.StartSynchronize(listingManager.listingsDictionary);
=======
    void StartSynchronize()
    {
        dataBaseCommunicator.StartSynchronize(listingManager.listingsDictionary);
>>>>>>> master2
    }

	private void AfterSynchronized(Dictionary<int, Listing> newListingsDictionary)
	{
		StartCoroutine(AfterSynchronizedCoroutine(newListingsDictionary));	
	}

	private IEnumerator AfterSynchronizedCoroutine(Dictionary<int, Listing> newListingsDictionary)
	{

		#if UNITY_ANDROID && !UNITY_EDITOR
		string[] listings = storageManager.ReadListingsMetafile();
		Debug.Log(">>> META FILE <<< ");
		foreach(string listing in listings)
		{
			Debug.Log(listing);
		}
		listingManager.AddListingsToList(listings);
		#endif

		listingManager.listingsDictionary = newListingsDictionary;
<<<<<<< HEAD
		//yield return StartCoroutine( listingManager.SaveListings() );
=======
		yield return StartCoroutine( listingManager.SaveListings() );
>>>>>>> master2
		networkManager.CreateServer ();
		Debug.Log (dataBaseCommunicator.agentId.ToString());
		ui.HideSyncScreen();
		ui.ShowListingMenu();
<<<<<<< HEAD
		yield return null;
=======
>>>>>>> master2
	}
	
    void OnServerInitialized()
    {
        Debug.Log("Server has been initialized.");
    }
	
	public void LoadPresentation(int listingId)
	{
		networkManager.LoadPresentationInClient (listingId);
		StartCoroutine(LoadPresentationToRAM(listingId));
	}

	private void LoadPresentationWithChoosenVantagePoint(int listingId, int vantagePointId)
	{
		Debug.Log ("Run remotely presentation of listing with ID: " + listingId);
		ui.ShowSyncScreen();
		ui.SetLoadingMessage();
		Listing currentListing = listingManager.listingsDictionary [listingId];
		listingManager.SetCurrentListing(currentListing);
		VantagePoint currentVantagePoint = listingManager.SetCurrentVantagePoint(vantagePointId);
		listingManager.LoadToRAMCurrentListingRenders ();
		currentVantagePoint.GoTo();//load hotspots for current vp 
		ui.HideSyncScreen();
		Run();
	}
	
	private IEnumerator LoadPresentationToRAM(int listingIndex)
	{
		Debug.Log ("Run presentation of listing: " + listingIndex);
		ui.ShowSyncScreen();
		ui.SetLoadingMessage();
		yield return new WaitForEndOfFrame ();
		Listing currentListing = listingManager.ListingAt(listingIndex);
		listingManager.SetCurrentListing(currentListing);
		int startVantagePointId = currentListing.startingVantagePointId;
		VantagePoint currentVantagePoint = listingManager.SetCurrentVantagePoint(startVantagePointId);
		listingManager.LoadToRAMCurrentListingRenders();
		currentVantagePoint.GoTo();
		//yield return new WaitForEndOfFrame ();
		ui.HideSyncScreen();
		//DataBaseCommunicator.Instance.GetFloorPlanPosition(listingIndex);
		DataBaseCommunicator.Instance.GetFloorPlanPosition(currentListing.Id);
		//listingManager.oldHotSpotList.Clear ();
		yield return new WaitForEndOfFrame ();
		Debug.Log (">>>>>>   !!!   TEST   !!!   <<<<<<");
		//DataBaseCommunicator database = new DataBaseCommunicator ();
		Debug.Log ("Current Listing ID from Main is = " + currentListing.Id);
		//yield return StartCoroutine (database.GetNewHotSpotId (currentListing.Id));
		DataBaseCommunicator.Instance.GetNew (currentListing.Id);
		yield return null;
//		Debug.Log ("loading HS ID: ");
//		foreach (int id in DataBaseCommunicator.Instance.idHotSpot)
//		{
//			Debug.Log("HotSpot id " + id);
//		}
		Run();
	}	

	public void Run()
	{
		//user choose EDIT or VIEW mode
		if(EditorManager.Instance.ModeEditON == true)
		{
			Debug.Log("Edit Mode Activated");
			editorManager.Activate();
		}
		if (EditorManager.Instance.ModeEditON == false)
		{
			Debug.Log("View Mode Activated");
			editorManager.ActivateView();
		}
	}
}
