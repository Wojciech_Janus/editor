﻿using UnityEngine;
using System.Collections;

public class SplittingString : MonoBehaviour 
{
	string money = "9999999999";
	// Use this for initialization
	void Start () 
	{
		money = AddComas (money);
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	string AddComas(string s)
	{
		int counter = 0;
		for(int i = s.Length-1 ;i>=0 ; i--)
		{
			if(counter==2)
			{
				s = s.Insert(i,",");
				counter=0;
			}
			else 
				counter++;
		}

		return s;
	}
}
