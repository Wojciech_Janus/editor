﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Represent Sphere. Contain references to renders images.
/// </summary>
[System.Serializable]
public class Sphere {

	private Image rightImage;
	private Image leftImage;
	
	public void Set(Image rightImage, Image leftImage)
	{
		this.rightImage = rightImage;
		this.leftImage = leftImage;
	}

	public Image RightImage
	{
		get { return rightImage; }
	}

	public Image LeftImage
	{
		get { return leftImage; }
	}

	public void Load()
	{
		Debug.Log(">>> Sphere.Load() <<< rightImage.TextureName: " + rightImage.TextureName + " leftImage.TextureName: " + leftImage.TextureName);
		RenderManager.Instance.SetRendersToSpheres(rightImage.TextureName, leftImage.TextureName);

	}

	public void LoadToRAM()
	{
<<<<<<< HEAD
		Debug.Log (">>> LoadToRAM() <<<");
=======
//		Debug.Log (">>> LoadToRAM() <<<");
>>>>>>> master2
		RenderManager.Instance.AddRendersToList(leftImage, rightImage);
	}

	public bool CheckIfObsolete(string newDate)
	{
		bool rightStatus = rightImage.IsModDataObsolete(newDate);
		bool leftStatus = leftImage.IsModDataObsolete(newDate);

		return (rightStatus && leftStatus);
	}
}
