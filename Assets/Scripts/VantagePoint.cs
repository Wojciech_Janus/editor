﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Http;

/// <summary>
/// Vantage Point representation. Full description data and access to helded HotSpots.
/// </summary>
[System.Serializable]
public class VantagePoint {

	//TODO:ADD variables to coroutine
	public int idVPInFloorPlan;
	public float xVPCoordinateInFloorPlan,yVPCoordinateInFloorPlan;
	public List<FloorPlanPoint> floorPlanPoint = new List<FloorPlanPoint>();
	//public List<FloorPlanData> floorPlanData = new List<FloorPlanData>();
	public List<HotSpotData> hotSpotsData = new List<HotSpotData>();

<<<<<<< HEAD
=======

>>>>>>> master2
	private const string HOTSPOTS_PATH = "/vantage_point/hotspots/";
	private const string HOTSPOT_KEY = "vantage_point_hotspots";

	private int LISTING_ID_KEY = 6;
	private int id;
	private string name;
	private int listingId;
	private Sphere sphere;

	public void Set(int id, string name, int listingId, Sphere sphere)
	{
		this.id = id;
		this.name = name;
		this.listingId = listingId;
		this.sphere = sphere;
	}

	public Sphere Sphere
	{
		get { return sphere; }
	}

	public int Id
	{
		get { return id; }
	}

	public string Name
	{
		get { return name; }
	}

	public string PreviewImageName
	{
		get { return sphere.RightImage.TextureName; }
	}

	public bool HasHotSpots()
	{
		Debug.Log ("VPs " + name + " has " + hotSpotsData.Count + " HS.");
		return (hotSpotsData.Count > 0);
	}

	public void AddHotSpotData(HotSpotData hotSpotDataToAdd)
	{
		Debug.Log("I ADD A HOTSPOT");// used in ListingManager
		this.hotSpotsData.Add(hotSpotDataToAdd);
<<<<<<< HEAD

		bool addOne = false;
		foreach (List <List<HotSpotData>> list_test  in ListingManager.Instance.allHotSpotsList[DataBaseCommunicator.Instance.currentListingIndex])
		{
			foreach (List <HotSpotData> vp_test in list_test)
			{
				if (addOne == false)
				{
					addOne = true;
					Debug.Log (">>>>>>>>hs to add to list<<<<<<<< " + hotSpotDataToAdd.id);
					vp_test.Add (hotSpotDataToAdd);
				}
			}
		}
=======
		//Debug.Log ("VP " + id + " ADD hot spot");//for current vantegepoint id
		//Debug.Log("X ="+hotSpotDataToAdd.x);
		//Debug.Log("Y ="+hotSpotDataToAdd.y);
		//Debug.Log("Distance ="+hotSpotDataToAdd.distance);
>>>>>>> master2
	}

	public void RemoveHotSpotData(HotSpotData hotSpotDataToRemove)
	{
		this.hotSpotsData.Remove(hotSpotDataToRemove);
<<<<<<< HEAD
		//Debug.Log ("VP " + id + " REMOVE hot spot");
=======
		Debug.Log ("VP " + id + " REMOVE hot spot");
>>>>>>> master2
	}

	public void GoTo()
	{
		NetworkManager.Instance.LoadVantagePointInClient (this.id);

		ListingManager.Instance.ClearHotSpotsList();
		Debug.Log("HotSpots after clear: " + ListingManager.Instance.hotSpotsList.Count);
<<<<<<< HEAD
		ShowHotSpots();
		sphere.Load();
=======

		ShowHotSpots();
		sphere.Load();

>>>>>>> master2
		ListingManager.Instance.SetCurrentVantagePoint(this);
	}

	private void ShowHotSpots()//how many HotSpot to Show
	{
			Debug.Log("ShowHotSpots() hotSpotsData.Count: " + hotSpotsData.Count);
			foreach(HotSpotData currentHotSpotData in hotSpotsData)//HotSpotData current= x,y,z ; hotSpotsData = List[]
			{
				GameObject hotSpotPrefab = EditorManager.Instance.hotSpotPrefab;
				
				GameObject hotSpotGO = UnityEngine.GameObject.Instantiate(hotSpotPrefab) as GameObject;

				HotSpot hotSpot = hotSpotGO.GetComponent<HotSpot>();
				hotSpot.SetData(currentHotSpotData);

				hotSpot.GoToDestination();
			}
<<<<<<< HEAD
		//foreach (HotSpot allHS in ListingManager.Instance.hotSpotsList)
		//{
		//	Debug.Log ("<HS> <ID> " + allHS.Data.id);
		//}
	}
	
	public IEnumerator LoadHotSpotFromDataBase(JSONObject hotSpotJson, int VP_index, int listing_index) 
=======
	}
	
	public IEnumerator LoadHotSpotFromDataBase(JSONObject hotSpotJson) 
>>>>>>> master2
	{
		//Debug.Log("HotSpots in Json: " +hotSpotJson);
		//Debug.Log ("Clear hotspots list");
		hotSpotsData.Clear();
		//ListingManager.Instance.oldHotSpotList.Clear ();
		//Debug.Log ("Load hotspots from database");
<<<<<<< HEAD
		//int howMuchHotSpotLoaded = 0;										//for debug only
		for (int i = 0; i<hotSpotJson.Count; i++)
		{
			//howMuchHotSpotLoaded++;											//for debug only
=======
		int howMuchHotSpotLoaded = 0;										//for debug only
		for (int i = 0; i<hotSpotJson.Count; i++)
		{
			howMuchHotSpotLoaded++;											//for debug only
>>>>>>> master2
			int id,linkedVantagePointID;				
			float x,y,distance;
			id = (int)hotSpotJson[i].GetField("id").n;
			x = hotSpotJson[i].GetField("angle_x").n;
			y = hotSpotJson[i].GetField("angle_y").n;
<<<<<<< HEAD
			//Debug.Log (" [i] " + i);
			//Debug.Log ("hs x = " + x);
			//Debug.Log ("hs y = " + y);
=======
			Debug.Log (" [i] " + i);
			Debug.Log ("hs x = " + x);
			Debug.Log ("hs y = " + y);
>>>>>>> master2
			distance = hotSpotJson[i].GetField("distance").n;
			linkedVantagePointID = (int)hotSpotJson[i].GetField("vantage_point_destination_id").n;

			Listing myListing = ListingManager.Instance.listingsDictionary[listingId];
			VantagePoint linkedVantagePoint = myListing.VantagePoints[linkedVantagePointID];
			HotSpotData readHotSpot = new HotSpotData(id,x,y,0,distance,this, linkedVantagePoint);
			ListingManager.Instance.oldHotSpotList.Add(readHotSpot);//add hotspot data to the list
			hotSpotsData.Add(readHotSpot);
<<<<<<< HEAD
			ListingManager.Instance.allHotSpotsList[listing_index][VP_index].Add(new List<HotSpotData>());//add new list for new hs
			ListingManager.Instance.allHotSpotsList[listing_index][VP_index][i].Add(readHotSpot);//add all hs for current lisitng
=======
>>>>>>> master2
		}
//		Debug.Log("How much hotspots loaded: " + howMuchHotSpotLoaded);		//Debug
		yield return null;
	}
}

