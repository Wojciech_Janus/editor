﻿using UnityEngine;
using System.Collections;

public class EditMode : Interactive {

	public Sprite editOff;
	public Sprite editOn;
	public GameObject deleteZone;

	void Start()
	{
		GetComponent<SpriteRenderer> ().sprite = editOff;
		EditorManager.Instance.ModeEditON = false;
		deleteZone.SetActive (false);
	}

	public override void OnChoose ()
	{
		Debug.Log ("Hit to change EDIT MODE");
		if (GetComponent<SpriteRenderer> ().sprite == editOff)
		{
			Debug.Log ("EDIT ON");
			GetComponent<SpriteRenderer> ().sprite = editOn;
			Change (true);
		}
		else
		{
			Debug.Log("EDIT OFF");
			GetComponent<SpriteRenderer> ().sprite = editOff;
			Change (false);
		}
	}

	private void Change (bool enable)
	{
		EditorManager.Instance.ModeEditON = enable;
		deleteZone.SetActive (enable);
	}

	public override void OnHold (){}
	public override void OnNotHold (){}
	public override void OnHover (){}
	public override void OnStopHover (){}
}
