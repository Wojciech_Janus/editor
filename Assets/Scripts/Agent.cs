﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Agent {

	public string name;
	public string surname;
	public string emailAdress;
	public string imgPath;

	public void SetInfo(string name = "", string surname = "", string emailAdress = "", string imgPath = "")
	{
		this.name = name;
		this.surname = surname;
		this.emailAdress = emailAdress;
		this.imgPath = imgPath;
	}

	public string Description
	{
		get {
					return name + "\n"
							+ surname + "\n"
							+ emailAdress + "\n";
				}

	}
}
