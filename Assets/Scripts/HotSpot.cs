﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Objects of this class is the interactive button which is using to change current VantagePoint (to linked one).
/// </summary>
public class HotSpot : Interactive {
	
	static private bool isSomeAttched;
	static public bool isOpenChooseMenu;

	public Text hotSpotID;
	public GameObject buttonDelete;
	public Text textDelete;

	#region MOVING_PARAMETERS
	private const float movingSpeed = -12.0f;
	private const float minDistanceToCamera = 2.5f;
	private const float maxDistanceToCamera = 50.0f;
	#endregion MOVING_PARAMETERS
	
	[SerializeField]
	private Color hoverColor;
	[SerializeField]
	private Color chooseColor;
	[SerializeField]
	private Color normalColor;
	private HotSpotData data;
	private SpriteRenderer spriteRenderer;
	private bool isOverDeleteZone;
<<<<<<< HEAD
	//private bool isHotSpotOnHotSpot;
	private float hs_x, hs_y, hs_distance;
=======
	private float hs_x, hs_y, hs_distance;

>>>>>>> master2
	private static HotSpot instance;

	public static HotSpot Instance
	{
		get { return instance; }
	}

<<<<<<< HEAD
=======


>>>>>>> master2
	public void Init (Vector3 inWorldPosition, float distance, VantagePoint myVantagePointl)
	{
		transform.LookAt(inWorldPosition);
		float x = transform.eulerAngles.x;
		float y = transform.eulerAngles.y;
		float z = transform.eulerAngles.z;
		this.data = new HotSpotData(x, y, z, distance, myVantagePointl);	
	}
	
	public HotSpotData Data
	{
		get { return data; }
	}

	public float Distance
	{
		get { return data.distance; }
	}

	private void CheckIfOverDeleteZone()
	{
		RaycastHit hitInfo;
		if(Physics.Raycast(transform.position, transform.forward, out hitInfo))
		{
			if(hitInfo.transform.name == "DeleteZone")//"DeleteZone")
				isOverDeleteZone = true;
			else
				isOverDeleteZone = false;
		}
	}

	public void SetData(HotSpotData data)
	{
		this.data = data;
	}

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.color = normalColor;
	}
	
	void Update()
	{
		CheckIfOverDeleteZone ();
		if (textDelete.text == "Deleting...")
		{
			Debug.Log ("deleting hot spot");
			isOverDeleteZone = true;
		}
	}

	public void SetLinkedVantagePoint(int index)
	{
		VantagePoint vpToLink = ListingManager.Instance.currentListing.VantagePointAt(index);
		if(vpToLink != null)
		{
			this.data.linkedVantagePoint = vpToLink;
			HotSpot.isOpenChooseMenu = false;
		}

		Activate();
	}

	public void GoToDestination()
	{
		//when click in edit mode - create Tail_VP
		Vector3 angles = new Vector3(data.x, data.y, data.z);
		transform.eulerAngles = angles;
		transform.position = transform.forward * data.distance;

		ListingManager.Instance.AddToHotSpotsList(this);

		if(data.linkedVantagePoint == null && !isOpenChooseMenu)
		{
			isOpenChooseMenu = true;
			Debug.Log("here disable ShowVantagePointMenu");
			//UI.Instance.ShowVantagePointMenu(this);
		}
	}
	
	public override void OnHover()
	{
		if(EditorManager.Instance.IsActive)
		{
<<<<<<< HEAD

												//buttonDelete.SetActive (true); to active the Delete Button on HotSpots
		}

		if (EditorManager.Instance.isFloorPlanOnScene == false)
		{
			try
			{
				spriteRenderer.color = hoverColor;
			}
			catch
			{
				Debug.LogError(">>> ERROR <<< HotSpot.OnHover()");
			}
=======
												//buttonDelete.SetActive (true); to active the Delete Button on HotSpots
		}


		try
		{
			spriteRenderer.color = hoverColor;
		}
		catch
		{
			Debug.LogError(">>> ERROR <<< HotSpot.OnHover()");
>>>>>>> master2
		}
	}
	
	public override void OnChoose()
	{
<<<<<<< HEAD
		if(EditorManager.Instance.isFloorPlanOnScene == false)
		{
			if(EditorManager.Instance.IsActive)
			{
				OnNotHold();
				Debug.Log("hotspot onchoose");
				//UI.Instance.ShowVantagePointMenu(this);
			}
			else
			{
				spriteRenderer.color = chooseColor;
				data.linkedVantagePoint.GoTo();
				Main.Instance.Run();
			}
		}
		//EditorManager.Instance.isFloorPlanOnScene = false;
		//EditorManager.Instance.windowForView = true;
		//Destroy(EditorManager.Instance.lastFloorPlan);
=======
		if(EditorManager.Instance.IsActive)
		{
			OnNotHold();
			Debug.Log("hotspot onchoose");
			//UI.Instance.ShowVantagePointMenu(this);
		}
		else
		{
			spriteRenderer.color = chooseColor;

			data.linkedVantagePoint.GoTo();

			Main.Instance.Run();
		}
		EditorManager.Instance.isFloorPlanOnScene = false;
		EditorManager.Instance.windowForView = true;
		Destroy(EditorManager.Instance.lastFloorPlan);
>>>>>>> master2
		//EditorManager.Instance.floorPlanData.Clear();//clear data selected floorplan
	}

	public override void OnHold ()
	{
<<<<<<< HEAD
		if(EditorManager.Instance.IsActive == true && EditorManager.Instance.isFloorPlanOnScene == false)
		{
			if (EditorManager.Instance.isHotSpotSelected == false)
			{
				AttachToPointer();
			}
=======
		if(EditorManager.Instance.IsActive)
		{
			AttachToPointer();
>>>>>>> master2

			Vector3 translation = ( transform.forward * (TouchInput.DeltaPosition.x * Time.deltaTime * movingSpeed));
			Vector3 newPosition = transform.position + translation;
			float distanceToCamera = Vector3.Distance(Vector3.zero, newPosition);

			if(distanceToCamera > minDistanceToCamera && distanceToCamera < maxDistanceToCamera)
			{
				transform.position = transform.position + translation;
				data.distance = distanceToCamera;
				data.SetAngles(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
				hs_x = transform.eulerAngles.x;
				hs_y = transform.eulerAngles.y;
				hs_distance = data.distance;
			}
		}
	}

	public override void OnNotHold ()
	{
		DeattachFromPointer();
 
		if(isOverDeleteZone)
			this.Remove();
<<<<<<< HEAD


	}

=======
	}
	
>>>>>>> master2
	public override void OnStopHover ()
	{
		if(EditorManager.Instance.IsActive)
		{
			buttonDelete.SetActive (false);
		}
		textDelete.color = Color.black;
		if (!isOverDeleteZone) 
		{
			try
			{
				DeattachFromPointer ();
				spriteRenderer.color = normalColor;
			}
			catch
			{
				Debug.LogError(">>> ERROR <<< HotSpot.OnStopHover()");
			}
		}
	}

	public void Deactivate()
	{
		spriteRenderer.enabled = false;
		GetComponent<BoxCollider>().enabled = false;
	}

	public void Activate()
	{
		spriteRenderer.enabled = true;
		GetComponent<BoxCollider>().enabled = true;
	}

	private void AttachToPointer()
	{
<<<<<<< HEAD
		EditorManager.Instance.isHotSpotSelected = true;
=======
>>>>>>> master2
		if(!isSomeAttched)
		{
			isSomeAttched = true;
			transform.SetParent(Pointer.Instance.transform);
		}
	}

	private void DeattachFromPointer()
	{
<<<<<<< HEAD
		EditorManager.Instance.isHotSpotSelected = false;
=======
>>>>>>> master2
		if(isSomeAttched)
		{
			isSomeAttched = false;
			transform.SetParent(null);
<<<<<<< HEAD
		}
	}

	public void Remove()
	{
		DataBaseCommunicator.Instance.RemoveHSFromList (data);
=======
			foreach (HotSpot hs in ListingManager.Instance.hotSpotsList)
			{
				if (hs.data.id == this.data.id)
				{
					Debug.Log (">>>STOP HOLD<<< find HS ID = " + hs.data.id);
					Debug.Log ("MY VALUES: x = " + hs.data.x + " y = " + hs.data.y + " z = " + hs.data.distance);
					//hs.data.x = hs_x;
					//hs.data.y = hs_y;
					//hs.data.distance = hs_distance;
					Debug.Log ("Values to save");
					Debug.Log ("NEW VALUES: x = " + hs.data.x + " y = " + hs.data.y + " z = " + hs.data.distance);
				}
			}
		}
	}
	public void Remove()
	{
>>>>>>> master2
		ListingManager.Instance.hotSpotsDeletedList.Add(data);
		data.myVantagePoint.RemoveHotSpotData(data);
		gameObject.SetActive(false);
		ListingManager.Instance.hotSpotsList.Remove(this);
		DestroyImmediate(gameObject);
	}
	void Start ()
	{
		SetTextID ();
	}

	public void SetTextID()
	{
//		Debug.Log ("myvantagepoint " +data.myVantagePoint.Id);
//		Debug.Log ("data linked " +data.linkedVantagePoint.Id.ToString());
		string destination = "destination " + data.linkedVantagePoint.Id.ToString ();
		string currentVP = "current " + data.myVantagePoint.Id.ToString ();
		hotSpotID.text = destination + "\n" + currentVP;
	}
}
