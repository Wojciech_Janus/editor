using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackButton : Interactive {
	
	private Button button;
	[SerializeField]
	private Text text;

	public override void OnChoose ()
	{
		if (EditorManager.Instance.IsActive == true)
		{//edit mode
			if (UI.Instance.isFloorPlanEdit == true)
			{//floorplan edit
				//BackToRoom ();
<<<<<<< HEAD
				StartCoroutine (BackToMenu ());
			}
			else
			{//floorplan view
				StartCoroutine (BackToMenu ());
=======
				BackToMenu ();
			}
			else
			{//floorplan view
				BackToMenu ();
>>>>>>> master2
			}

		}
		else
		{//view mode = floorplan view
<<<<<<< HEAD
			StartCoroutine (BackToMenu ());
		}
	}

	private IEnumerator BackToMenu ()
	{
		Debug.Log("Back To Menu");
		Camera.main.transform.parent.rotation = Quaternion.Euler(0, 0, 0);
		EditorManager.Instance.Close();
		yield return null;
=======
			BackToMenu ();
		}
	}

	private void BackToMenu ()
	{
		Debug.Log("Back To Menu");
		UI.Instance.buttonEditMode.SetActive (true);
		Camera.main.transform.parent.rotation = Quaternion.Euler(0, 0, 0);
		EditorManager.Instance.Close();
		EditorManager.Instance.floorPlanData.Clear();//clear data selected floorplan
		DataBaseCommunicator.Instance.urlFP.Clear ();
		DataBaseCommunicator.Instance.nameFP.Clear ();
		DataBaseCommunicator.Instance.idHotSpot.Clear ();
		//ListingManager.Instance.floorPlanData.Clear ();
		//ListingManager.Instance.oldHotSpotList.Clear ();
		ListingManager.Instance.hotSpotsList.Clear ();
		Back ();
>>>>>>> master2
	}

	public void BackToRoom ()
	{
		Debug.Log ("Back");
		Back ();
	}

	private void Back ()
	{
		EditorManager.Instance.isFloorPlanOnScene = false;
		EditorManager.Instance.windowForView = true;
		Destroy(EditorManager.Instance.lastFloorPlan);
	}

	public override void OnHold (){}
	public override void OnNotHold (){}

	public override void OnHover ()
	{
		text.color = Color.white;
	}
	
	public override void OnStopHover ()
	{
		text.color = Color.blue;
	}
}
