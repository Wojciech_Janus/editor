﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EditButton : Interactive {

	//public bool windowForEdit = false;//to choose windows QuestionBoxEdit
	//public bool windowForView = false;//to choose windows QuestionBoxView -> FloorPlan

	private Toggle toggle;

	void OnEnable()
	{
		toggle = GetComponentInChildren<Toggle> ();
		SwitchOff ();
	}

	public override void OnChoose ()
	{
		//if (Tail.hoverScaleSet == false)
		//{
			if (toggle.isOn == true)
			{
				SwitchOff();
			}
			else if(toggle.isOn == false)
			{
				SwitchOn();
			}
		//}
	}
	public override void OnHold ()
	{

	}
	public override void OnNotHold ()
	{

	}
	public override void OnHover ()
	{

	}
	public override void OnStopHover ()
	{

	}

	private void SwitchOff()
	{
		Switch (false);
	}

	private void SwitchOn()
	{
		Switch (true);
	}

	private void Switch(bool isEnable)
	{
		toggle.isOn = isEnable;
		EditorManager.Instance.ModeEditON = isEnable;
		
		EditorManager.Instance.windowForView = !isEnable;
		EditorManager.Instance.windowForEdit = isEnable;
	}
}
