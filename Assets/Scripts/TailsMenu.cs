﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class TailsMenu<T> {

	public Action OnClose;

	private UI ui;
	private Transform transfrom;
	private int numberOfTails;

	public Transform Transform
	{
		get { return transfrom; }
	}

	public TailsMenu()
	{
		ui = UI.Instance;

		transfrom = (MonoBehaviour.Instantiate(ui.tailsMenuPreab) as GameObject).transform;

		numberOfTails = 0;
	}

	public void AddTail(T linkedObject, Action<int> OnTailChoose, string name, Texture texture)
	{
		RectTransform tail_vp = (MonoBehaviour.Instantiate(ui.tailPrefab_VP) as GameObject).GetComponent<RectTransform>();
		Tail_VP tailObj =  tail_vp.GetComponent<Tail_VP>();

		tailObj.Init(numberOfTails, transfrom, OnTailChoose, name, texture); //TODO add img path
		tail_vp.SetParent(transfrom);

		++numberOfTails;
	}
	
}
