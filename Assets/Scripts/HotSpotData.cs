﻿
/// <summary>
/// Represent HotSpot serializable information.
/// </summary>

[System.Serializable]
public class HotSpotData {
	
	public int id;
	public float x;
	public float y;
	public float z;
	public float distance;
	public VantagePoint myVantagePoint;
	public VantagePoint linkedVantagePoint;

	public HotSpotData (float x, float y, float z, float distance, VantagePoint myVantagePoint)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.distance = distance;
		this.myVantagePoint = myVantagePoint;
	}

	public HotSpotData (int id, float x, float y, float z, float distance, VantagePoint myVantagePoint, VantagePoint linkedVantagePoint)
	{
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.distance = distance;
		this.myVantagePoint = myVantagePoint;
		this.linkedVantagePoint = linkedVantagePoint;
	}

	public void SetAngles(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
