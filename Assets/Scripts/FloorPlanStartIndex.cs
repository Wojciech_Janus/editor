﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloorPlanStartIndex : MonoBehaviour {

	public Text floorPlanIndexText;

	void Start () 
	{
		int now = EditorManager.Instance.nowIndexOfFloorPlan;
		int max = DataBaseCommunicator.Instance.maxIndexOfFloorPlan;
		floorPlanIndexText.text = now + " / " + max;
	}

}
