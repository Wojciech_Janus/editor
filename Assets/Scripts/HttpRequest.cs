﻿using System.Net;
using System.IO;

namespace Http
{
	public enum HttpMethod
	{
		GET,
		POST,
		PATCH,
		DELETE
	};

	public class HttpRequest {

		private HttpMethod method;
		private string url;
		private string token;
		private string json;
		private string result;

		public HttpRequest(HttpMethod method, string url, string token = "")
		{
			this.method = method;
			this.url = url;
			this.token = "Token token=" + token;
			this.json = "";
			this.result = "";
		}

		public void AddField(string key, string value)
		{
			if(json.Equals(""))
			{
				json = ("{\"" + key + "\":\"" + value + "\"");
			}
			else
			{
				json += (",\"" + key + "\":\"" + value + "\"");
			}

		}

		public void AddField(string key, float value)
		{
			if(json.Equals(""))
			{
				json = ("{\"" + key + "\":" + value);
			}
			else
			{
				json += (",\"" + key + "\":" + value);
			}
		}

		public string Send()
		{
			json += "}";

			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + ".json");//"​.json"

			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Method = method.ToString();

			if(!token.Equals(""))
			{
				httpWebRequest.Headers["AUTHORIZATION"] = token;
			}

			if(method == HttpMethod.POST)
			{
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					UnityEngine.Debug.Log("JSON: " + json);
					streamWriter.Write(json);
					streamWriter.Flush();
					streamWriter.Close();
				}
			}

			if (method == HttpMethod.PATCH)
			{
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					UnityEngine.Debug.Log("JSON: " + json);
					streamWriter.Write(json);
					streamWriter.Flush();
					streamWriter.Close();
				}
			}

			if (method == HttpMethod.DELETE)
			{
				//using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				//{
				//	UnityEngine.Debug.Log("JSON: " + json);
					//streamWriter.Write(json);
					//streamWriter.Flush();
				//	streamWriter.Close();
				//}
			}
			
			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
			{
				result = streamReader.ReadToEnd();
			}
			return result;
		}
	}
}
