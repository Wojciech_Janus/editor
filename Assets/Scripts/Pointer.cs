﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represent VR Pointer. Sending raycast and invoking Interactiva class events.
/// </summary>
public class Pointer : MonoBehaviour {

	private static Pointer instance;
	private Interactive currentInteractiveObject;
	private Vector3 hitPosition;

	public delegate void OnRaycastHitDelegate(Vector3 hitPosition);
	public event OnRaycastHitDelegate OnRaycastHit;

	void Awake()
	{
		if(instance == null)
			instance = this;
	}

	public static Pointer Instance
	{
		get { return instance; }
	}

	void Update()
	{
		if (isPoitingToInteractiveItem())
		{
			if(TouchInput.Hold)
				currentInteractiveObject.OnHold();
			else if(!TouchInput.SingleTap)
				currentInteractiveObject.OnNotHold();

			currentInteractiveObject.OnHover();
			WaitForUserAction();
		}

	}

	private bool isPoitingToInteractiveItem()
	{
		bool status;
		Interactive io = SendRaycastAndGetInteractiveObject();
		if(io == null)
		{
			if(TouchInput.SingleTap)
			{
				Debug.Log("OnPoiterRaycastHit");
				OnRaycastHit(hitPosition);
			}
			if(currentInteractiveObject != null)
			{
				currentInteractiveObject.OnStopHover();
				currentInteractiveObject = null;
			}
		}
		else
		{
			currentInteractiveObject = io;
		}
		status = (currentInteractiveObject != null);
		return status;
	}

	private Interactive SendRaycastAndGetInteractiveObject()
	{
		RaycastHit hitInfo;
		if(Physics.Raycast(transform.position, transform.forward, out hitInfo))
		{
			hitPosition = hitInfo.point;
			return hitInfo.transform.gameObject.GetComponent<Interactive>();
		}
		else
			return null;
	}

	private void WaitForUserAction()
	{
		StartCoroutine (WaitForUserActionCoroutine ());
	}

	private IEnumerator WaitForUserActionCoroutine()
	{
		while(currentInteractiveObject != null)
		{
			if(TouchInput.SingleTap) //if ray is in tail position 
			{
				currentInteractiveObject.OnChoose();
				currentInteractiveObject = null;
			}
			yield return new WaitForEndOfFrame();
		}
	}
}