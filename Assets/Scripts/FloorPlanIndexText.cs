﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloorPlanIndexText : MonoBehaviour {

	public Text floorPlanIndexText;

	void Start () 
	{
		int now = EditorManager.Instance.nowIndexOfFloorPlan;
		int max = DataBaseCommunicator.Instance.maxIndexOfFloorPlan-1;
		floorPlanIndexText.text = now + " / " + max;
	}
}
