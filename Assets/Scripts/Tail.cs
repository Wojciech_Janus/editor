using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tail : Interactive {
	
	//public static bool hoverScaleSet = false;
	
	public int currentTail;
	
	private const float NORMAL_SCALE_SCALAR = 1.0f;
	private const float HOVER_SCALE_SCALAR = 1.1f;
	private const float HOVER_END_SCALAR = 1.1f;
	private float scale;
	
	
	public bool isOpened = false;
	[SerializeField]
	
	private System.Action<int> OnChooseAction;
	
	[SerializeField]
	private Text nameText;
	[SerializeField]
	private Text priceText;
	[SerializeField]
	private Text descriptionTextl;
	[SerializeField]
	private Text agentDescription;
	[SerializeField]
	private UnityEngine.UI.Image agentImage;
	[SerializeField]
	private GameObject myTailsMenu;
	[SerializeField]
	private Renderer imageRenderer;
	private Material material;
	public int listingIndex;
	public int index;
	private int tailHover;		
	private int lastIndex;
	private ScrollingMenu scrollingMenu;
	public Animator anim;
	
	
	void Start()
	{
		scale = transform.localScale.x;
		anim = GetComponent<Animator> ();
		scrollingMenu = ScrollingMenu.GetMenu ();
		
		
		float finishColor = 1f/(Mathf.Abs(index - scrollingMenu.currentTail)+1);
		imageRenderer.material.color = new Color(finishColor, finishColor, finishColor);
		
		
		if(index == 0)
		{
			isOpened = true;
			anim.SetBool("animation", true);
			scrollingMenu.MoveAside(true);
		}
	}

	public void Init(int index, int listingIndex, Transform myTailsMenu, System.Action<int> OnChooseAction, string name, Texture texture, Agent agent, string price = "", string description = "", string status = "")
	{
//		Debug.Log ("Created Tail with index: " + index + ", listingIndex: " + listingIndex + " and name: " + name);//menu tail in room
		this.listingIndex = listingIndex;
		this.index = index;
		this.myTailsMenu = myTailsMenu.gameObject;
		this.OnChooseAction += OnChooseAction;
		this.nameText.text = name;

/*
		string finalPrice = status.ToUpper () + ": " + price;

		for (int i = 0; i < finalPrice.Length; i++) 
		{
			if(i%3 == 0)
			{
				finalPrice = finalPrice.Insert(i, ",");
			}
		}

		this.priceText.text = status.ToUpper() + ": " + price;
		
		this.descriptionTextl.text = description;
		
		agentDescription.text = agent.Description;
*/
		/*
		if(!agent.imgPath.Equals(""))
		{
			Texture2D agentTexture = StorageManager.Instance.ReadTexture2DFromMemory(agent.imgPath);
			Sprite agentSprite= Sprite.Create(agentTexture, new Rect(0, 0,agentTexture.width, agentTexture.height), new Vector2(0.5f, 0.5f));
		
			agentImage.sprite = agentSprite;
		}
		*/
		this.CloneMaterialAndSetTexture(texture);
		lastIndex = index;
		
	}
	
	public override void OnChoose ()
	{
		if(!isOpened)
		{
			if(index == scrollingMenu.currentTail)
			{
				/*
				Debug.Log("EDIT MODE IS: "+EditorManager.Instance.isEdit);
				EditorManager.Instance.isEditButtonDelete = true;
				OnChooseAction(index);
				currentTail = index;
				Close();
				*/
				//isOpened = true;
				//scrollingMenu.openedIndex = index;
				//scrollingMenu.MoveAside(true);
				//anim.SetBool("animation", true);
				
			}
			else
			{
				//scrollingMenu.ChangeFocus(index);
			}
		}
	}
	
	public void StartFading(float duration)
	{
		StartCoroutine(SmoothdFading(duration));
	}
	
	public IEnumerator SmoothdFading(float duration)
	{
		float startTime = Time.time;
		float dTime = 0.0f;
		float currentColor = 0;
		int currentIndex = scrollingMenu.currentTail;
		
		float startingColor = imageRenderer.material.color.r;
		float finishColor = 1f/(Mathf.Abs(index - currentIndex)+1);
		
//		Debug.Log (startingColor + "fnish: " + (index - currentIndex));
		
		while(dTime < duration)
		{
			currentColor = Mathf.Lerp(startingColor, finishColor,(dTime/duration));
			imageRenderer.material.color = new Color(currentColor, currentColor, currentColor);
			yield return null;
			dTime = Time.time - startTime;
		}
	}
	
	public void TakeTour()
	{
		//Debug.Log("EDIT MODE IS: "+EditorManager.Instance.isEdit);
		//EditorManager.Instance.isEditButtonDelete = true;
		//OnChooseAction(listingIndex);
		currentTail = index;
		Close();
	}
	
	public override void OnHold ()
	{
		
	}
	
	public override void OnHover ()
	{
		//if(scrollingMenu.currentTail != index)
		//transform.localScale = Vector3.one * HOVER_SCALE_SCALAR;
		//transform.localScale = new Vector3 (scale * HOVER_SCALE_SCALAR, scale * HOVER_SCALE_SCALAR, 1);
		//transform.position = new Vector3(transform.position.x,transform.position.y, zPosition * HOVER_END_SCALAR);
		//hoverScaleSet = true;
	}
	
	public override void OnNotHold ()
	{
		
	}
	
	public override void OnStopHover ()
	{
		//if(isOpened && scrollingMenu.currentTail != index)
		//transform.localScale = new Vector3(scale ,scale ,1);
		//transform.localPosition = new Vector3(transform.localPosition.x,transform.position.y, zPosition);
		//hoverScaleSet = false;
		
	}
	
	private void CloneMaterialAndSetTexture(Texture texture)
	{
		material = new Material(imageRenderer.material);
		material.mainTexture = texture;
		imageRenderer.material = material;
	}
	
<<<<<<< HEAD
	private void Close()//private
=======
	public void Close()//private
>>>>>>> master2
	{
		Debug.Log ("Now i will close the VP rendering list");
		UI.Instance.isSomeWindowOpen = false;
		if (myTailsMenu.name.Contains ("ScrollingMenu")) 
		{
			Debug.Log (">>>Close ScrollingMenu<<<");
			UI.Instance.isListingMenuOpen = false;
		}
		myTailsMenu.SetActive(false);
		Destroy(myTailsMenu);
		UI.Instance.ShowSyncScreen ();
		OnChooseAction (listingIndex);
	}
}
