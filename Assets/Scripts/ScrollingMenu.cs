﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Scrolling menu. First Menu-List rendering
/// </summary>
public class ScrollingMenu : MonoBehaviour {
	
	private const float TAIL_ANGLE =17;
	private const float TAIL_SHIFT = 3.0f;
	private const float MOVE_TIME = 0.35f;
	
	private static ScrollingMenu menu;
	private int numberOfTails = 0;
	public int currentTail = 0;
	private static List<Tail> tailList;
	public bool isScrolling = false;
	public int openedIndex = -1;
	private float startedAngle = 0;
	
	public static ScrollingMenu Inst()
	{
		GameObject scrollingMenu = Instantiate(UI.Instance.scrollingMenuPrefab) as GameObject;
		tailList = new List<Tail> ();
		menu = scrollingMenu.GetComponent<ScrollingMenu>();
		return menu;
	}
	
	public static ScrollingMenu GetMenu()
	{
		return menu;
	}
	
	public void AddTail(Action<int> OnTailChoose, string name, string price, string description, Texture texture, int listingIndex, Agent agent, string status)
	{
//		Debug.Log ("AddTail at START");
		RectTransform tail = (MonoBehaviour.Instantiate(UI.Instance.tailPrefab) as GameObject).GetComponent<RectTransform>();
		Tail tailObj =  tail.GetComponent<Tail>();
		
		tailObj.Init(numberOfTails, listingIndex, transform, OnTailChoose, name, texture, agent, price, description, status); //TODO add img path
		//tail.position = GetTailPosition();
		tail.position = new Vector3 (0, 0, 0);
		tail.SetParent(transform);
		
		tail.eulerAngles = new Vector3 (0, TAIL_ANGLE * numberOfTails, 0);
		
		
		tailList.Add (tailObj);
//		Debug.Log (">>ADD TAIL<<< tailArray count: " + tailList.Count);
		
		++numberOfTails;
	}
	void Start()
	{
		transform.position = new Vector3 (0, 0, 0);
		//transform.eulerAngles = new Vector3 (0, Camera.main.transform.eulerAngles.y, 0);
		transform.eulerAngles = new Vector3 (0, 0, 0);
		transform.position = -transform.forward * 3;
		startedAngle = transform.eulerAngles.y;
		//openedIndex = -1;
		currentTail = 0;
	}
	
	void Update()
	{
		if(openedIndex == -1)
		{
			openedIndex = 0;
			foreach(Tail tail in tailList)
			{
				tail.StartFading(MOVE_TIME);
			}
		}
		//transform.eulerAngles = new Vector3 (0, 0, 0);
		//transform.position = new Vector3 (0, 0, -3);
		if (TouchInput.Right && !isScrolling && currentTail>0) 
		{
			if(openedIndex !=-1)
			{
				SwipeTails(-1);
			}
			else if(openedIndex == -1)
			{
				isScrolling = true;
				MoveRight();
			}
			
			
			
			
		}
		else if (TouchInput.Left && !isScrolling && currentTail < numberOfTails-1) 
		{
			if(openedIndex !=-1)
			{
				SwipeTails(1);
			}
			else if(openedIndex==-1)
			{
				isScrolling = true;
				MoveLeft ();
			}
		} 
		if (Input.GetKeyDown (KeyCode.Escape) && openedIndex != -1) 
		{
			//Debug.Log (openedIndex);
			//tailList[openedIndex].anim.SetBool("animation", false);
			//tailList[openedIndex].isOpened = false;
			//MoveAside(false);
			//openedIndex = -1;
		}
		
	}
	
	public void SwipeTails(int orientation) // 1  left    -1  right
	{
		
		
		tailList[openedIndex].anim.SetBool("animation", false);
		tailList[openedIndex].isOpened = false;
		openedIndex += orientation;
		tailList[openedIndex].anim.SetBool("animation", true);
		tailList[openedIndex].isOpened = true;
		isScrolling = true;
		currentTail += orientation;
		MoveOpened();
		
		foreach(Tail tail in tailList)
		{
			tail.StartFading(MOVE_TIME);
		}
		
	}
	
	public void MoveAside(bool outside)
	{
		if(outside)
			foreach(Tail tail in tailList)
		{
			if(tail.index < currentTail)
				StartCoroutine( MoveTail (currentTail+0.5f ,tail.transform) );
			else if(tail.index > currentTail)
				StartCoroutine( MoveTail (currentTail-0.5f ,tail.transform) );
		}
		else
			foreach(Tail tail in tailList)
		{
			if(tail.index < currentTail)
				StartCoroutine( MoveTail (currentTail ,tail.transform) );
			else if(tail.index > currentTail)
				StartCoroutine( MoveTail (currentTail ,tail.transform) );
		}
	}
	
	
	
	public void ChangeFocus(int index)
	{
		if(openedIndex !=-1)
		{
			try{
				tailList[openedIndex].anim.SetBool("animation", false);
				openedIndex =-1;
			}
			catch
			{
				Debug.LogError(">>> ERROR <<< ScrollingMenu.ChangeFocus(" + index + ") openedIndex: " + openedIndex);
			}
		}
		tailList[currentTail].isOpened = false;
		currentTail = index;
		tailList[currentTail].isOpened = true;
		foreach(Tail tail in tailList)
			StartCoroutine( MoveTail (currentTail ,tail.transform) );
	}
	
	
	private void MoveOpened()
	{
		if( currentTail < numberOfTails)
		{
			foreach(Tail tail in tailList)
			{
				if(tail.index < currentTail)
					StartCoroutine( MoveTail (currentTail +0.5f,tail.transform) );
				else if(tail.index == currentTail)
					StartCoroutine( MoveTail (currentTail ,tail.transform) );
				else if(tail.index > currentTail)
					StartCoroutine( MoveTail (currentTail -0.5f,tail.transform) );
			}
			
		}
		else
			isScrolling = false;
	}
	
	private void MoveRight()
	{
		if( currentTail > 0)
		{
			currentTail--;
			foreach(Tail tail in tailList)
				StartCoroutine( MoveTail (currentTail ,tail.transform) );
		}
		else
			isScrolling = false;
	}
	
	private void MoveLeft()
	{
		if(currentTail < numberOfTails-1)
		{
			currentTail++;
			foreach(Tail tail in tailList)
				StartCoroutine( MoveTail (currentTail ,tail.transform) );
		}
		else
			isScrolling = false;
	}
	
	
	
	private Vector3 GetTailPosition()
	{
		Vector3 direction = transform.right;
		Vector3 position = direction * TAIL_SHIFT * numberOfTails;
		return position;
	}
	
	private IEnumerator Move(int orientation)
	{
		/*
		Vector3 startPosition = transform.position;
		Vector3 position = new Vector3 ((transform.position.x+(TAIL_SHIFT*orientation)),transform.position.y,transform.position.z);

		float startTime = Time.time;
		float dTime = 0.0f;

		while(dTime < MOVE_TIME)
		{
			transform.position = Vector3.Lerp(startPosition, position, (dTime/MOVE_TIME));
			yield return null;
			dTime = Time.time - startTime;
		}
		*/
		return null;
	}
	
	
	
	private IEnumerator MoveTail(float destiny ,Transform tail)
	{
		//Vector3 startPosition = tail.position;
		//Vector3 position = new Vector3 (TAIL_SHIFT*(tail.GetComponent<Tail>().index-destiny),tail.position.y,tail.position.z);
		
		Quaternion startRotation = tail.rotation;
		Quaternion endRotation = Quaternion.Euler (0, startedAngle + TAIL_ANGLE * (tail.GetComponent<Tail> ().index - destiny), 0);
		
		float startTime = Time.time;
		float dTime = 0.0f;
		
		
		while(dTime < MOVE_TIME)
		{
			//tail.position = Vector3.Lerp(startPosition, position, (dTime/MOVE_TIME));
			tail.rotation = Quaternion.Lerp(startRotation, endRotation, (dTime/MOVE_TIME));
			yield return null;
			dTime = Time.time - startTime;
		}
		isScrolling = false;
	}
}
