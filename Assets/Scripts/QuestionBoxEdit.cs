﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class QuestionBoxEdit : MonoBehaviour {

	public VRButton closeAndSave;
	public VRButton close;
	public VRButton cancel;
	public Text question;

	public static GameObject Inst(Action OnYes, Action OnNo, Action OnClose, string question)
	{
		GameObject questionBoxGO = Instantiate(UI.Instance.questionBoxPrefabEdit) as GameObject;
		QuestionBoxEdit questionBox = questionBoxGO.GetComponent<QuestionBoxEdit>();

		questionBox.closeAndSave.Set(questionBoxGO, OnYes);
		questionBox.close.Set(questionBoxGO, OnNo);
		questionBox.cancel.Set(questionBoxGO, OnClose);
		questionBox.question.text = question;

		return questionBoxGO;
	}

	void Update()
	{
		/*if(Input.GetKeyDown(KeyCode.Escape))
		{
			gameObject.SetActive(false);
			DestroyImmediate(gameObject);
		}*/
	}
}
